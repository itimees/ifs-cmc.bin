---------------------------------------------------------------------------
-- Embedded phone service Test Client
---------------------------------------------------------------------------

local service = require "service"
local assert = assert
local json = require "json"

--module(..., package.seeall)

local ServiceName = "com.harman.service.EmbeddedPhone"

-- Enable/Disable prints from this service
local function nothing() return true end
---------------------------------------------------------------------------
-- Wrappers around svcipc
---------------------------------------------------------------------------
local function invoke(method, params)
   local res = assert(service.invoke(ServiceName, method, params,60000))
   return res
end

---------------------------------------------------------------------------
--methods
---------------------------------------------------------------------------
function setEmbeddedPhoneStatus(status)
   local res = {}
   res = invoke("setEmbeddedPhoneStatus",{status = status})
end
function dial(number)
   local res = {}
   res = invoke("dial",{number = number})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
      print("Call_Id       :"..res.callId)
   end
end
function reDial()
   local res = {}
   res = invoke("reDial",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
   end
end
function ecallDial(address,callType,vehicleType)
   local res = {}
   res = invoke("ecallDial",{address=address,callType=callType,vehicleType=vehicleType})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
      print("Call_Id       :"..res.callId)
   end
end
function writeTtyStr(ttyString)
   local res = {}
   res = invoke("writeTtyStr",{ttyString=ttyString})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("respCode      :"..res.respCode)
   end
end
function acceptIncomingCall()
   local res = {}
   res = invoke("acceptIncomingCall",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
   end
end
function endActiveCall()
   local res = {}
   res = invoke("endActiveCall",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
   end
end
function endCall(callId)
   local res = {}
   res = invoke("endCall",{callId = callId})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
   end
end
function rejectIncomingCall()
   local res = {}
   res = invoke("rejectIncomingCall",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Reason        :"..res.reason)
   end
end
function restoreFactorySettings()
   local res = {}
   res = invoke("restoreFactorySettings",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getAntennaStatus(antenna)
   local res = {}
   res = invoke("getAntennaStatus",{antenna = antenna})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("status        :"..res.status)
   end
end
function getTemperature()
   local res = {}
   res = invoke("getTemperature",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Temperature   :"..res.temperature)
      print("maxTemperature   :"..res.maxTemperature)
   end
end
function getPaTemperature()
   local res = {}
   res = invoke("getPaTemperature",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Temperature   :"..res.temperature)
      print("maxTemperature   :"..res.maxTemperature)
   end
end
function debugAT(atString)
   local res = {}
   res = invoke("debugAT",{atString = atString})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function exitEmergencyMode()
   local res = {}
   res = invoke("exitEmergencyMode",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getWENDReason()
   local res = {}
   res = invoke("getWENDReason",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("wendReason   :"..res.wendReason)
      print("serviceNumber   :"..res.serviceNumber)
   end
end
function getEmergencyNumbers(maxCount)
   local res = {}
   res = invoke("getEmergencyNumbers",{maxCount = maxCount})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("numberList   :")
	  for _,v in ipairs(res.numberList) do
		print(v)
	  end
   end
end
function startUpdate(dataSize)
   local res = {}
   res = invoke("startUpdate",{dataSize=dataSize})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function transferUpdateData(data)
   local res = {}
   res = invoke("transferUpdateData",{data=data})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function finishUpdate()
   local res = {}
   res = invoke("finishUpdate",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function resetPhone()
   local res = {}
   res = invoke("resetPhone",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getSubscriberNumber(maxCount)
   local res = {}
   res = invoke("getSubscriberNumber",{maxCount=maxCount})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("count:"..res.count)
	  for _,v in ipairs(res.numberList) do
		print("number:"..v)
	  end
	  for _,v in ipairs(res.numberTypeList) do
		print("numberType:"..v)
	  end
	  for _,v in ipairs(res.serviceList) do
		print("service:"..v)
	  end
   end
end
function getSwRevision()
   local res = {}
   res = invoke("getSwRevision",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("swApplVer     :"..res.swApplVer)
      print("swBootVer     :"..res.swBootVer)
   end
end
function getMEID()
   local res = {}
   res = invoke("getMEID",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("MEID          :"..res.imei)
      print("ESN          :"..res.esn)
   end
end
function getIMSI()
   local res = {}
   res = invoke("getIMSI",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("IMSI          :"..res.imsi)
   end
end
function getDevTime()
   local res = {}
   res = invoke("getDevTime",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("devTime          :"..res.devTime)
   end
end
function getDevStatus()
   local res = {}
   res = invoke("getDevStatus",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("devStatus          :"..res.devStatus)
   end
end
function getServingSystem()
   local res = {}
   res = invoke("getServingSystem",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("bandClass          :"..res.bandClass)
      print("band          :"..res.band)
      print("baseStationProtocolRev          :"..res.baseStationProtocolRev)
      print("sid          :"..res.sid)
      print("rfChNumber          :"..res.rfChNumber)
   end
end
function getModel()
   local res = {}
   res = invoke("getModel",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("Model         :"..res.model)
      print("hwVersion     :"..res.hwVersion)
   end
end
function getSerialNumber()
   local res = {}
   res = invoke("getSerialNumber",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("SerialNumber     :"..res.serialNumber)
   end
end
function sendDtmf(sequence)
   local res = {}
   res = invoke("sendDtmf",{sequence = sequence})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function sendSms(number, smsBody, priority)
   local res = {}
   res = invoke("sendSms",{number=number,smsBody=smsBody,priority=priority})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getNewSms(queryId,storageType)
   local res = {}
   res = invoke("getNewSms",{queryId=queryId,storageType=storageType})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      if res.smsList then
	  for _,v in ipairs(res.smsList) do
		print("From:"..v.smsFrom)
		if v.smsLocalRead == true then
		print("LocalRead:True")
		else
		print("LocalRead:False")
		end
		print("Body:"..v.smsBody)
		print("timeStamp:"..v.timeStamp)
		print("queryId:"..v.queryId)
	  end
	  end
   end
end
function getPendingSms(appId,destPort)
   local res = {}
   res = invoke("getPendingSms",{appId=appId,destPort=destPort})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      if res.smsList then
	  for _,v in ipairs(res.smsList) do
		print("From:"..v.smsFrom)
		if v.smsLocalRead == true then
		print("LocalRead:True")
		else
		print("LocalRead:False")
		end
		print("Body:"..v.smsBody)
		print("timeStamp:"..v.timeStamp)
		print("queryId:"..v.queryId)
	  end
	  end
   end
end
function deleteSms(status,queryId)
   local res = {}
   res = invoke("deleteSms",{status=status,queryId=queryId})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function registerSmsPort(appId,destPort)
   local res = {}
   res = invoke("registerSmsPort",{appId = appId,destPort=destPort})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function unregisterSmsPort(appId,destPort)
   local res = {}
   res = invoke("unregisterSmsPort",{appId = appId,destPort=destPort})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function openDataConnection(number,name,username,password)
   local res = {}
   res = invoke("openDataConnection",{number=number,name=name,username=username,password=password})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function closeDataConnection(name)
   local res = {}
   res = invoke("closeDataConnection",{name=name})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getProperties(props)
   local res = {}
   res = invoke("getProperties",{props = props})
   if res then
--      for k,v in pairs(res) do
--         for x,y in pairs(v) do
--            print(k.." : { "..x.." : "..y.." }")
--         end
--      end
   print(json.encode(res))
   end
end
function getProfileInformation(profileNumber)
   local res = {}
   res = invoke("getProfileInformation",{profileNumber=profileNumber})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("nai   :"..res.profileInfo.nai)
      print("HA_IPAdress_home   :"..res.profileInfo.HA_IPAdress_home)
      print("HA_IPAdress_primary   :"..res.profileInfo.HA_IPAdress_primary)
      print("HA_IPAdress_secondary   :"..res.profileInfo.HA_IPAdress_secondary)
      print("MN_AAA_SharedSecretSPI   :"..res.profileInfo.MN_AAA_SharedSecretSPI)
      print("MN_HA_SharedSecretSPI   :"..res.profileInfo.MN_HA_SharedSecretSPI)
      print("MN_AAA_SharedSecretKey   :"..res.profileInfo.MN_AAA_SharedSecretKey)
      print("MN_HA_SharedSecretKey   :"..res.profileInfo.MN_HA_SharedSecretKey)
   end
end
function selectProfile(profileNumber)
   local res = {}
   res = invoke("selectProfile",{profileNumber=profileNumber})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getSIDNID(maxCount)
   local res = {}
   res = invoke("getSIDNID",{maxCount=maxCount})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("sidNid   :"..res.sidNid)
   end
end
function getPrlVersion()
   local res = {}
   res = invoke("getPrlVersion",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
      print("prlVersion   :"..res.prlVersion)
   end
end
function setProvisioning(spc,imsi,mdn,nai,MN_AAA_SharedSecretKey,MN_AAA_SharedSecretSPI,MN_HA_SharedSecretKey,MN_HA_SharedSecretSPI,HA_IPAdress_primary,HA_IPAdress_secondary,HA_IPAdress_home,rescan_interval,diagnose_port)
   local res = {}
   res = invoke("setProvisioning",{spc=spc
									,imsi=imsi
									,mdn=mdn
									,nai=nai
									,MN_AAA_SharedSecretKey=MN_AAA_SharedSecretKey
									,MN_AAA_SharedSecretSPI=MN_AAA_SharedSecretSPI
									,MN_HA_SharedSecretKey=MN_HA_SharedSecretKey
									,MN_HA_SharedSecretSPI=MN_HA_SharedSecretSPI
									,HA_IPAdress_primary=HA_IPAdress_primary
									,HA_IPAdress_secondary=HA_IPAdress_secondary
									,HA_IPAdress_home=HA_IPAdress_home
									,rescan_interval=rescan_interval
									,diagnose_port=diagnose_port
									})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
   end
end
function getUsbPorts()
   local res = {}
   res = invoke("getUsbPorts",{})
   if res then
      print("Code          :"..res.code)
      print("Description   :"..res.description)
	  for _,v in ipairs(res.ports) do
	      print("ports   : "..v)
	  end
   end
end
function getMdn()
   local res = {}
   res = invoke("getMdn",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("mdn     		  :"..res.mdn)
   end
end
function getPri()
   local res = {}
   res = invoke("getPri",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("pri     		  :"..res.pri)
   end
end
function getNai()
   local res = {}
   res = invoke("getNai",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("nai     		  :"..res.nai	)
   end
end
function getReverseTunneling()
   local res = {}
   res = invoke("getReverseTunneling",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("reverseTunneling :"..res.reverseTunneling)
      print("commit     	  :"..res.commit)
   end
end
function getHspi()
   local res = {}
   res = invoke("getHspi",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("hspi     		  :"..res.hspi)
      print("commit     	  :"..res.commit)
   end
end
function getAspi()
   local res = {}
   res = invoke("getAspi",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("aspi     		  :"..res.aspi)
      print("commit     	  :"..res.commit)
   end
end
function getMobileStationIp()
   local res = {}
   res = invoke("getMobileStationIp",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("ip     		  :"..res.ip)
   end
end
function getPrimaryHaAddress()
   local res = {}
   res = invoke("getPrimaryHaAddress",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("address     	  :"..res.address)
      print("commit     	  :"..res.commit)
   end
end
function getSecondaryHaAddress()
   local res = {}
   res = invoke("getSecondaryHaAddress",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("address     	  :"..res.address)
      print("commit     	  :"..res.commit)
   end
end
function getAccessOverloadClass()
   local res = {}
   res = invoke("getAccessOverloadClass",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("aoc     		  :"..res.aoc)
   end
end
function getSlotCycleIndex()
   local res = {}
   res = invoke("getSlotCycleIndex",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("sci     		  :"..res.sci)
   end
end
function getMnHaSharedSecrets()
   local res = {}
   res = invoke("getMnHaSharedSecrets",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("mnhaCode     	  :"..res.mnhaCode)
   end
end
function getMnAaaSharedSecrets()
   local res = {}
   res = invoke("getMnAaaSharedSecrets",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("mnaaaCode     	  :"..res.mnaaaCode)
   end
end
function getAccessPointName()
   local res = {}
   res = invoke("getAccessPointName",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
      print("apn     		  :"..res.apn)
      print("dssAction     	  :"..res.dssAction)
   end
end
function getCdmaSystemStatus()
   local res = {}
   res = invoke("getCdmaSystemStatus",{})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
	  for k,v in pairs(res.status) do
		print(k)
		for _,u in ipairs(v) do
			print("........"..u)
		end
	  end
   end
end

function setImsi(imsi)
   local res = {}
   res = invoke("setImsi",{imsi = imsi})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setMdn(mdn)
   local res = {}
   res = invoke("setMdn",{mdn = mdn})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setNai(nai,commit)
   local res = {}
   res = invoke("setNai",{nai = nai,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setReverseTunneling(reverseTunneling,commit)
   local res = {}
   res = invoke("setReverseTunneling",{reverseTunneling = reverseTunneling, commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setHspi(hspi,commit)
   local res = {}
   res = invoke("setHspi",{hspi = hspi,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setAspi(aspi,commit)
   local res = {}
   res = invoke("setAspi",{aspi = aspi,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setPrimaryHaAddress(address,commit)
   local res = {}
   res = invoke("setPrimaryHaAddress",{address = address, commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setSecondaryHaAddress(address,commit)
   local res = {}
   res = invoke("setSecondaryHaAddress",{address = address,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setAccessOverloadClass(aoc)
   local res = {}
   res = invoke("setAccessOverloadClass",{aoc = aoc})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setSlotCycleIndex(sci)
   local res = {}
   res = invoke("setSlotCycleIndex",{sci = sci})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setMnHaSharedSecrets(mnhaCode,commit)
   local res = {}
   res = invoke("setMnHaSharedSecrets",{mnhaCode = mnhaCode,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setMnAaaSharedSecrets(mnaaaCode,commit)
   local res = {}
   res = invoke("setMnAaaSharedSecrets",{mnaaaCode = mnaaaCode,commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setSpc(spc)
   local res = {}
   res = invoke("setSpc",{spc = spc})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setRtn(spc)
   local res = {}
   res = invoke("setRtn",{spc = spc})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function commitChanges(commit)
   local res = {}
   res = invoke("commitChanges",{commit = commit})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
function setAccessPointName(apn,username,password)
   local res = {}
   res = invoke("setAccessPointName",{apn = apn,username = username,password = password})
   if res then
      print("Code             :"..res.code)
      print("Description      :"..res.description)
   end
end
-------------------------------------------------------------------------
--event handlers
-------------------------------------------------------------------------
local function embeddedPhoneStatusHandler(signal,param)
   print("Signal              : "..signal)
   if param.status == true then
      print("status                : true")
   else
      print("status                : false")
   end
end
local function callStateHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("CallStateInfo: ")
   if param and param.callStateInfo then
      for _,v in ipairs(param.callStateInfo) do
         print("callId           : "..v.callId)
         print("callState        : "..v.callState)
         print("callType         : "..v.callType)
         print("number           : "..v.number)
      end
   end
end
local function incomingCallInfoHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("CallInfo            : ")
   if param  then
      print("callId              : "..param.callId)
      print("number              : "..param.number)
   end
end
local function signalQualityHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("rssi                : "..param.rssi)
end
local function batteryChargeHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("Level               : "..param.level)
end
local function networkRegistrationStateHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("State               : "..param.state)
end
local function networkOperatorChangedHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("OperatorCode        : "..param.operatorCode)
   print("OperatorLongName    : "..param.operatorLongName)
   print("OperatorShortName   : "..param.operatorShortName)
   print("OperatorMode        : "..param.operatorMode)
   print("OperatorAccTech     : "..param.operatorAccTech)
end
local function resetHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("Dump                : "..param.dump)
end
local function updateProgressHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("Percentage          : "..param.percentage)
end
local function hwStateHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("State          : "..param.hwState)
end
local function wendReasonHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("reason               : "..param.reason)
   print("serviceNumber               : "..param.serviceNumber)
end
local function wcntReasonHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("reason               : "..param.reason)
end
local function antennaStatusHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("antenna         : "..param.antenna)
   print("Status              : "..param.status)
end
local function tempStateHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("State          : "..param.state)
end
local function pmicTempChangedHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("temperature          : "..param.temperature)
end
local function paTempChangedHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("temperature          : "..param.temperature)
end
local function newSmsHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("QueryId             : "..param.queryId)
   print("destPort         : "..param.destPort)
end
local function deviceServiceStateHandler(signal,param)
   print("=========================================================")
   print("Signal              : "..signal)
   print("---------------------------------------------------------")
   print("event             : "..param.event)
   print("data         : "..param.data)
end
-------------------------------------------------------------------------
local function onEmbPhoneServiceAvailable(newName, oldOwner, newOwner)
   print("EmbPhoneService available, subscribing event")
   subEmbeddedPhoneStatus        = service.subscribe(ServiceName, "embeddedPhoneStatus", embeddedPhoneStatusHandler)
   subCallState                  = service.subscribe(ServiceName, "callState", callStateHandler)
   subIncomingCallInfo           = service.subscribe(ServiceName, "incomingCallInfo", incomingCallInfoHandler)
   subSignalQuality              = service.subscribe(ServiceName, "signalQuality", signalQualityHandler)
   subBatteryCharge              = service.subscribe(ServiceName, "batteryCharge", batteryChargeHandler)
   subNetworkRegistrationState   = service.subscribe(ServiceName, "networkRegistrationState", networkRegistrationStateHandler)
   subNetworkOperatorChanged     = service.subscribe(ServiceName, "networkOperatorChanged", networkOperatorChangedHandler)
   subReset                      = service.subscribe(ServiceName, "reset", resetHandler)
   subUpdateProgress             = service.subscribe(ServiceName, "updateProgress", updateProgressHandler)
   subHwStatus                   = service.subscribe(ServiceName, "hwState", hwStateHandler)
   subTempState                  = service.subscribe(ServiceName, "tempState", tempStateHandler)
   subPmicTempChanged            = service.subscribe(ServiceName, "pmicTempChanged", pmicTempChangedHandler)
   subPaTempChanged              = service.subscribe(ServiceName, "paTempChanged", paTempChangedHandler)
   subNewSms                     = service.subscribe(ServiceName, "newSms", newSmsHandler)
   subWendReason                 = service.subscribe(ServiceName, "wendReason", wendReasonHandler)
   subWendReason                 = service.subscribe(ServiceName, "wcntReason", wcntReasonHandler)
   subAdcDataItemStatus          = service.subscribe(ServiceName, "antennaStatus", antennaStatusHandler)
   subDeviceServiceStatus        = service.subscribe(ServiceName, "deviceServiceState", deviceServiceStateHandler)
end
local function display()
   print("-------------------------------------------------")
   print("               M     E     N     U               ")
   print("-------------------------------------------------")
   print(" (1) -> getProperties()")
   print(" (2) -> setEmbeddedPhoneStatus()")
   print(" (3) -> dial()")
   print(" (4) -> reDial()")
   print(" (5) -> acceptIncomingCall()")
   print(" (6) -> endCall()")
   print(" (7) -> endActiveCall()")
   print(" (8) -> rejectIncomingCall()")
   print(" (9) -> restoreFactorySettings()")
   print("(10) -> getAntennaStatus()")
   print("(11) -> getTemperature()")
   print("(12) -> startUpdate()")
   print("(13) -> transferUpdateData()")
   print("(14) -> finishUpdate()")
   print("(15) -> resetPhone()")
   print("(16) -> getSubscriberNumber()")
   print("(17) -> getSwRevision()")
   print("(18) -> getMEID()")
   print("(19) -> getModel()")
   print("(20) -> getSerialNumber()")
   print("(21) -> sendDtmf()")
   print("(22) -> sendSms()")
   print("(23) -> getNewSms()")
   print("(24) -> deleteSms()")
   print("(25) -> openDataConnection()")
   print("(26) -> closeDataConnection()")
   print("(27) -> Exit test client")
   print("-------------------------------------------------")
end
local function menu()
   Quit = false
   while Quit == false do 
      display() 
      print'Enter your API choice:'
      choice = io.stdin:read'*l'
      print(".................................................")
      if choice == "1" then
         getProperties()
      elseif choice == "2" then
         setEmbeddedPhoneStatus()
      elseif choice == "3" then
         print("Enter Phone number")
         number = io.stdin:read'*l'
         dial(number)
      elseif choice == "4" then
         reDial()
      elseif choice == "5" then
         acceptIncomingCall()
      elseif choice == "6" then
         print("Enter call_Id")
         call_Id = io.stdin:read'*n'
         endCall(call_Id)
      elseif choice == "7" then
         endActiveCall()
      elseif choice == "8" then
         rejectIncomingCall()
      elseif choice == "9" then
         restoreFactorySettings()
      elseif choice == "10" then
         getAntennaStatus()
      elseif choice == "11" then
         getTemperature()
      elseif choice == "12" then
         startUpdate()
      elseif choice == "13" then
         transferUpdateData()
      elseif choice == "14" then
         finishUpdate()
      elseif choice == "15" then
         resetPhone()
      elseif choice == "16" then
         getSubscriberNumber()
      elseif choice == "17" then
         getSwRevision()
      elseif choice == "18" then
         getMEID()
      elseif choice == "19" then
         getModel()
      elseif choice == "20" then
         getSerialNumber()
      elseif choice == "21" then
         print("Enter DTMF sequence")
         dtmf = io.stdin:read'*l'
         sendDtmf(dtmf)
      elseif choice == "22" then
         sendSms()
      elseif choice == "23" then
         getNewSms()
      elseif choice == "24" then
         deleteSms()
      elseif choice == "25" then
         openDataConnection()
      elseif choice == "26" then
         closeDataConnection()
      elseif choice == "27" then
         Quit = true
      else
         print("Invalid choice")
      end
   end
end

local function start()
   embPhoneSubscription = assert(service.subscribeOwnerChanged(ServiceName, onEmbPhoneServiceAvailable))
   if service.nameHasOwner(ServiceName) then
      onEmbPhoneServiceAvailable("x", "y", "z") -- need to change xyz if we check the args
   end
   --menu()
   --os.exit()
end

start()