--[[
Update is triggerd by mcd detecting a file "swdl.upd" on a usb device.

The update service does the following:
* Authenticate swdl.upd (using the new fast digest algorighm)
* Mount swdl.upd at /fs/swdl
* Run the lua script "/fs/swdl/manifest"
* The script returns a table called the manifest. This table has some fields that drive the update.
* Check the manifest to see if it contains an update for the current part number.
* The update service sends out a signal to the HMI to prompt the user for the update.
* The HMI gets the version information from the update service
* If the users says "yes," reset the system into update mode

In update mode:
* Start only the drivers and services necessary for the update
* Wait for usb insertion detection
* As before, authenticate, mount and load the manifest
* Find this part number which is an array of units
* Each unit contains an installer. This is a lua module that is either on the radio or inside swdl.upd
* Install each unit
* Prompt user to remove the usb device
* Reset into normal mode

The manifest can actually do anything it wants and return an empty table.
At that point the update is done. Or the manifest can do a reset and never return.
]]



--[[ "loader" module

This module is used by both the resident and installer scripts.

It contains functions for:
   - detecting, mounting and loading update manifest
   - authentication     
]]

local mcd      = require "mcd"
local string   = require "string"
local io       = io
local os       = os
local assert   = assert
local ipairs   = ipairs
local loadfile = loadfile
local pairs    = pairs
local pcall    = pcall
local print    = print
local type     = type
local lfs    = require "lfs"

local onoff     = require "onoff"


module(...)

VERSION = "0.1"

trace = print

--------------------------------------------------------------------------------
-- local variables

config = {} -- make separate module if used in multiple projects

-- MCD rules
config.mcdInsertRule = "SWDL"
config.mcdEjectRule  = "EJECTED"

-- Default file and directory names
config.isoName       = "swdl.upd"      -- name of the swdl iso file
config.isoMount      = "/fs/swdl"      -- mountpoint for iso
config.installerIsoMount      = "/fs/installer"      -- mountpoint for installerIsoMount
config.manifestName  = "etc/manifest.lua"      -- name of the manifiest within the iso
installerIsoName = "installer.iso"
primaryIsoName = "primary.iso"
secondaryIsoName = "secondary.iso"
config.splitIsoNames = {installerIsoName, primaryIsoName, secondaryIsoName}

config.keysFile      = "/etc/keys/swdl.pub"


-- These are set to the directory names after mounting
usbDir = nil
isoDir = nil
installerIsoDir = nil

-- path for putting the extracted keys and digest

extractedPrimKey        = "/tmp/isokey.pem"
extractedIsoDigest      = "/tmp/isodigest.sha256"
createdIsoDigest        = "/tmp/iso.digest"

local ignore_mcd_event = false
hdr_cfg      = {}

--------------------------------------------------------------------------------
-- Function to read the ISO Header Information (MY, Variant etc)
-- @param none
--------------------------------------------------------------------------------

local function get_iso_build_info()
   local t = {isomajver = nil, isominver = nil, build_year = nil, build_week = nil, build_patch = nil, model_year = nil, market = nil, variant = nil, isosize = nil}
   
   local f = io.open("/tmp/buildinfo.bin", "r")
   
   if f then
      local r = f:seek("set", 0)
      if r and r == 0 then 
         local hdr = f:read(28)
         if hdr and hdr ~= 0 then                     
            t.isomajver = string.sub(hdr, 1, 1)
            t.isominver = string.sub(hdr, 2,2)
            t.build_year = string.sub(hdr, 3, 4)
            t.build_week = string.sub(hdr, 5, 6)
            t.build_patch = string.sub(hdr, 7, 8)
            t.model_year = string.sub(hdr, 9, 12)
            t.market = string.sub(hdr, 13, 14)
            t.variant = string.sub(hdr, 15 , 17)
            t.isosize = string.sub(hdr, 18, 28)
		end
      end
      f:close()
   end
   
   return t
end

--------------------------------------------------------------------------------
-- cleanTmpFiles - Function to delete hash and header related temporary files
--------------------------------------------------------------------------------
local function cleanTmpFiles()
   --remove the temp files
   os.execute("rm -f /tmp/32k.bin")
   os.execute("rm -f /tmp/32kdigsig.bin")
   os.execute("rm -f /tmp/isoheader.bin")
   os.execute("rm -f /tmp/buildinfo.bin")
   os.execute("rm -f /tmp/isohash.bin")
   os.execute("rm -f /tmp/calculatedisohash.bin")
   os.execute("rm -f /tmp/extractedisohash.bin")
   os.execute("mount -uw /fs/mmc0")
   os.execute("rm -f /fs/mmc0/installer.iso")
   os.execute("mount -ur /fs/mmc0")   
end

-------------------------------------------------------------------------------
-- getISOBuildVersion - Return build version from ISO Header 
--------------------------------------------------------------------------------
function getISOBuildVersion()
    return hdr_cfg.build_year.."."..hdr_cfg.build_week.."."..hdr_cfg.build_patch   
end    

--------------------------------------------------------------------------------
-- getCurrentVersion from version file on target 
--------------------------------------------------------------------------------
local function getCurrentVersion()
    local temp
	local fromVersion
    local f = io.open("/etc/version.txt", "r")
    if not f then 
          fromVersion = "UNKNOWN"        
    else
        for line in f:lines() do                
            temp =  string.match(line,"^%s*version%s*=%s*(%C*)") 
            if temp then                 
                fromVersion = string.upper(temp)                
            break
            end                    
        end 
        f:close() 
    end
	return fromVersion
end  

--------------------------------------------------------------------------------------------------
-- getDeltaTargetVersion from /fs/mmc3/update/FOTA_COMPLETE_STATUS/deltaTargetVersion if available 
--------------------------------------------------------------------------------------------------
local function getDeltaTargetVersion()
   local deltaTargetVersion=nil

   -- Check if deltaTargetVersion file present or not, if present then get the version
   local f = io.open("/fs/mmc3/update/FOTA_COMPLETE_STATUS/deltaTargetVersion", "r")
   if f then 
      deltaTargetVersion = f:read("*all")
      print("[SWDL] deltaTargetVersion=", deltaTargetVersion)
      f:close()
   end

   return deltaTargetVersion
end  


--------------------------------------------------------------------------------
-- Function to get file present status
-- @param dir file directory path (optional, defaults to the usb mount point)
-- @param name file name 
--------------------------------------------------------------------------------
function file_exists(dir, name)
  dir = assert(dir or usbDir)
  local filePath = dir.."/"..name

  -- Check if source file exists
  local cmd
  local file_present = true
  cmd = "ls -l "..filePath
  print(cmd)   
  
  local f = assert (io.popen (cmd, "r")) 
  local lines = f:read("*l") 
  print(lines)
  if (lines == nil) then     
     file_present = false      
  end   
  f:close()
  
  return file_present
end

--------------------------------------------------------------------------------
-- Set auto update status flag in fram.
-- 	The updstatus value will mean one of the following
--      0 - No Update
--		1 - Stick Ejected/not inserted 
--      2 - Stick Inserted
--      3 - Update in progress - No means to report this while HU is updating
--      4 - SW update complete - done
--      5 - same version - done
--      7 - variant mistmatch
--      8 - Model Year mismatch
--      9 - Authentication error - done
--------------------------------------------------------------------------------
function setAutoUpdateStatus(offset, updstatus)
	local n
	local swdlStateFile = "/dev/mmap/swdl"
	local fd, err = io.open(swdlStateFile, "w")
	if fd then	
		fd:seek("set", offset)
		n, err = fd:write(updstatus)
		fd:close()
	end
	return n == 1, err
end

--------------------------------------------------------------------------------
-- Authenticate iso image
-- @param dir is the directory of the iso (optional, defaults to the usb mount point)
-- @param splitIsoName in the case of split iso need splitted iso names for authentication 
--------------------------------------------------------------------------------

function authenticateISO(dir, splitIsoName)

    local flag = 0
    dir = assert(dir or usbDir)

    local isopath = dir.."/"..splitIsoName

    trace("authenticating "..isopath)
   
     -- Copy the 32K header to /tmp 
   if(0 ~= os.execute("dd if="..isopath.." of=/tmp/32k.bin ibs=32768 count=1 > /dev/null 2>&1")) then
      return nil, "Failed to copy ISO Header"
   end

   -- Copy the ISO Header Dig Signature (first 256 bytes) from ISO to /tmp 
   if(0 ~= os.execute("dd if=/tmp/32k.bin of=/tmp/32kdigsig.bin ibs=256 count=1 > /dev/null 2>&1")) then
      return nil, "Failed to copy Digital Signature"
   end

   -- Copy the ISO Header Dig Signature (32K less the first 256 bytes) from ISO to /tmp 
   if(0 ~= os.execute("dd if=/tmp/32k.bin of=/tmp/isoheader.bin ibs=256 count=127 skip=1 > /dev/null 2>&1")) then
      return nil, "Failed to copy ISO Header Dig Signature"
   end

   -- Extract the ISO Information portion of the header (MY, Variant etc)
   if(0 ~= os.execute("dd if=/tmp/32k.bin of=/tmp/buildinfo.bin ibs=256 count=1 skip=1 > /dev/null 2>&1")) then
      return nil, "Failed to copy ISO Information from Header"
   end
 
   local key_file = config.keysFile
   -- authenticate ISO Header with Digital Signature
   print("key_file "..key_file)
   cmd = "openssl dgst -sha256 -verify "..key_file.." -signature /tmp/32kdigsig.bin /tmp/isoheader.bin"
   print(cmd)   
   local f = assert (io.popen (cmd, "r"))  
   for line in f:lines() do
      print(line)
      if (string.find(line, "Verified OK") ~= nil) then 
         print( " Able to verify the usb stick" )
         flag = 1          
         break
      end                     
   end 
   f:close()
	
	if flag==1 then 
	   print("ISO Verified, continue...")	   
	else        
	   -- report authentication failure autoupdate status bit
	   setAutoUpdateStatus(50,9)
	   return nil, "Failed to Authenticate ISO"
	end
	 

      hdr_cfg = get_iso_build_info()
	  print("ISO SIZE IS ", hdr_cfg.isosize)
      if(hdr_cfg.isomajver and hdr_cfg.isomajver ~= "") then

	     if(hdr_cfg.isomajver ~= "1" or hdr_cfg.isominver ~= "0") then
            print("Failed to read ISO Header")
            return nil, "Failed to read ISO Header"
         end
	
         local system_variant = os.getenv("VARIANT_PRODUCT") 
         if((system_variant == nil) or (system_variant == "")) then 
            print("Loader.lua: Unable to find environment variable VARIANT_PRODUCT")
           return nil, "Loader.lua: Unable to find environment variable VARIANT_PRODUCT"
         elseif(system_variant ~= hdr_cfg.variant) then
            print(" Loader.lua: System product mismatch, stick contains %s but target is %s ", hdr_cfg.variant, system_variant)
			setAutoUpdateStatus(50,7)
            return nil, "Loader.lua: System product mismatch, stick contains "..hdr_cfg.variant.." but target is "..system_variant
         end

         local system_market = os.getenv("VARIANT_MARKET") 
         if((system_market == nil) or (system_market == "")) then 
            print("Loader.lua: Unable to find environment variable VARIANT_MARKET")
            return nil, "Loader.lua: Unable to find environment variable VARIANT_MARKET"
         else
	
            translated_env_value = string.upper(system_market)
            if(translated_env_value == "ECE") then
               translated_env_value = "EU"
            elseif(translated_env_value == "ROW") then
               translated_env_value = "RW"
            end
		
            if(translated_env_value ~= hdr_cfg.market) then
               print(" Loader.lua: System market mismatch, stick contains %s but target is %s ", hdr_cfg.market, system_market)
               return nil, " Loader.lua: System market mismatch, stick contains "..hdr_cfg.market.." but target is "..system_market
            end
         end		 
        
		 local system_modelyear = os.getenv("VARIANT_MODELYEAR")
         if((system_modelyear == nil) or (system_modelyear == "")) then
            print("Loader.lua: Unable to find environment variable VARIANT_MODELYEAR")
            return nil, "Loader.lua: Unable to find environment variable VARIANT_MODELYEAR"
         elseif(("MY17" ~= hdr_cfg.model_year) and (system_modelyear ~= hdr_cfg.model_year)) then
            print("Loader.lua: Model year mismatch. This software update is incompatible with this radio. Update cannot be performed remove USB stick to return radio to normal operation.Visit DriveUconnect.com for correct software")
            setAutoUpdateStatus(50,8)
			return nil, "Loader.lua: Model year mismatch. This software update is incompatible with this radio. Update cannot be performed remove USB stick to return radio to normal operation.Visit DriveUconnect.com for correct software"
         end
       
      local currentSWVersion = getCurrentVersion()
      local isoBuildVersion  = getISOBuildVersion()
      local deltaTargetVersion = getDeltaTargetVersion()
      
      if(string.find(currentSWVersion, "TRUNK")) then
        currentSWVersion = "00.00.00"
      end     
        
      -- Checking for a later or the same version of build.
      -- This allows Dev build to upgrade to Dev Build (00.00.00 <= 00.00.00)
      if(currentSWVersion <= isoBuildVersion) then
        print("Build version condition is satisfied")
      else
        return nil, "USB contains Older SW and Head Unit is running Newer SW"   
      end

      -- Check if FOTA delta available if yes then check for latest SW & allow only latest SW
      if (deltaTargetVersion ~= nil) then
         if (deltaTargetVersion <= isoBuildVersion) then
            print("USB contains New SW and FOTA has Old SW")
         else
            return nil, "USB contains Older SW and FOTA has Newer SW"
         end
      end

      -- ISO is verified removed all temporary files
      print("ISO Verified...")

      else
	     print("Failed to read ISO Version")
		 return nil, "Failed to read ISO Version"
	  end

	  -- Basic checks are done by now, verify the entire installer.iso
     if(splitIsoName == installerIsoName) then
	  os.execute("mount -uw /fs/mmc0")
      if(0 ~= os.execute("dd if="..isopath.." of=/fs/mmc0/installer.iso ibs=1048576 > /dev/null 2>&1")) then
         os.execute("mount -ur /fs/mmc0")
		 return nil, "Failed to copy Installer ISO"
      end
     isopath="/fs/mmc0/installer.iso"
	  os.execute("mount -ur /fs/mmc0")
	  
      -- Extract the Hash of ISO Data portion 
      if(0 ~= os.execute("dd if="..isopath.." of=/tmp/isohash.bin ibs=256 count=1 skip=127 > /dev/null 2>&1")) then
         return nil, "Failed to extract ISO hash"
      end
      if(0 ~= os.execute("openssl rsautl -verify -inkey /etc/keys/swdl.pub -in /tmp/isohash.bin -pubin -out /tmp/extractedisohash.bin")) then
         print("Loader: Failed to verify ISO Hash")
         return nil, "Failed to verify ISO hash"
      end
                              
      -- get the hash of the ISO updating the progress as we go
      fcmd,error = io.popen("hashFile sha256 "..isopath.." /tmp/calculatedisohash.bin 32768 0 0 0", "r")
      if not fcmd or error then
         return nil, "hashFile returned an error when executed"
      end
      
      for line in fcmd:lines() do
         -- handle error
         if ( string.match(string.upper(line), "^%s*ERROR" ) ~= nil) then
            return nil, "hashFile returned an error while checking Installer"
         end   
      end
      fcmd:close()
   
      -- if ISO-verified and calculated hashes are the same, then PASS, otherwise fail
      if(0 == os.execute("cmp -s /tmp/calculatedisohash.bin /tmp/extractedisohash.bin")) then
         print("ISOCHK: ISO Data section hash and hash in header match")
      else   
         print("Installer Data section hash and hash in header don't match")
         return nil, "Installer Data section hash and hash in header don't match"         
      end
	  
	 end
	  
   return true
end   
   

--------------------------------------------------------------------------------
-- Mount iso image
-- Return the iso mount point or nil and error message.
-- @param dir is the directory of the iso (optional, defaults to the usb mount point) 
-- @param isoName iso name
-- @param isoMount and iso mount path
function mountISO(dir, isoName, isoMountPath)
   dir = assert(dir or usbDir)
   if (isoName == nil) or (isoMountPath == nil) then
      return nil
   end
   
   local path = dir.."/"..isoName
   trace("mounting "..path)
   local ok, err = os.mount(path, isoMountPath, "r", "cd", "exe")
   if not ok then return nil, err end
   return isoMountPath
end

function umountISO(isoMountPath)
   if isoMountPath then
      os.umount(isoMountPath, "f")
      isoMountPath = nil
   end
end

--------------------------------------------------------------------------------
-- Load the update manifest script and return the manifest (a table).
-- If an error occurs, return nil and an error message.
-- @param path is an optional path to the manifest

function loadManifest(path)
   path = path or (installerIsoDir.."/"..config.manifestName)

   -- load the manifest
   trace("loading manifest "..path)
   local chunk, error = loadfile(path)
   if not chunk then
      trace("error loading manifest:", error)
      return nil, error
   end

   -- run the manifest which should return a table
   trace("calling manifest")
   local ok, manifest = pcall(chunk)
   if not ok then
      trace("error running manifest:", manifest)
      return nil, manifest
   end
   
   -- make sure manifest is a table
   if type(manifest) ~= "table" then
      -- error or warning?
      trace("manifest did not return a table")
      return {}
   end

   return manifest
end


--------------------------------------------------------------------------------
-- Notification of update media insert/eject.
-- On insertion, the callback function is called with the manifest or nil and an error message

function notifyOnInsert(callback)
    local function inserted(path)
        if ignore_mcd_event then 
            print(" Ignoring mcd insert event ")
            return {}            
        end     
            
      local manifest, ok, err
      trace("update media inserted:", path)
	  setAutoUpdateStatus(50,2)
      usbDir = path

      -- check if this is the ONLY USB inserted
      -- if not, ask HMI to prompt user to remove all
      -- and insert only the desired update
      if not string.match(path, "usb0") then
	     -- 1 = USB inserted
		 -- 11 = multiple USB
		 setAutoUpdateStatus(50,22)
         return callback(nil, "Multiple USB devices detected.  Please remove all devices and re-insert only update device.")
      end
     
      -- mount iso
      isoDir, err = mountISO(usbdir, config.isoName, config.isoMount)
      if not isoDir then
         trace("mount iso failed:", err)
         return callback(nil, "mount iso failed")
      end
     
      -- Media insertion detected and mounted
      local detected = true
      -- call back to display the popup as in CR#5323
      callback(manifest, "Insertion detected and media is mounted",detected)
     
      -- authenticate iso
      for i = 1, #config.splitIsoNames do
      
         if file_exists(isoDir, config.splitIsoNames[i]) then 
         
            ok, err = authenticateISO(isoDir, config.splitIsoNames[i])
			cleanTmpFiles()
            if not ok then
               trace("authenticate iso failed:", config.splitIsoNames[i], err)
               return callback(nil, err)
            end
         else
            if (config.splitIsoNames[i] == installerIsoName) then
               trace("installer.iso Not available")
               return callback(nil, "installer.iso Not available")
            end
         end
      end
   
      -- mount iso
      installerIsoDir, err = mountISO(isoDir, installerIsoName, config.installerIsoMount)
      if not installerIsoDir then
         trace("mount iso failed:", err)
         return callback(nil, "mount installerIsoDir failed")
      end

      -- load manifest
      manifest, err = loadManifest()
      if not manifest then
         trace("load manifest failed:", err)
         return callback(nil, "load manifest failed")
      end

      -- give manifest to callback
      callback(manifest, path)
   end
   mcd.notify(config.mcdInsertRule, inserted)
end

--------------------------------------------------------------------------------
-- On eject, unmount the iso and call callback

function notifyOnEject(callback)
   local function ejected(path)
        if ignore_mcd_event then 
            print(" Ignoring mcd eject events ")
            return {}            
        end     
        print(" path "..path)
        if ( string.find(path, "usb" ) ~= nil) then     
            trace("update media ejected")
			setAutoUpdateStatus(50,1)
            umountISO(isoDir) -- won't this happen automtically?
            umountISO(installerIsoDir) -- won't this happen automtically?
            isoDir = nil
            installerIsoDir = nil
            usbDir = nil
            callback()
        end    
   end
   mcd.notify(config.mcdEjectRule, ejected)
end


function executeExternalScript(script)
    local cmd = nil

    if (not isoDir) or (not usbDir) or (not installerIsoDir) then 
        print("Either mount iso dir or usb dir or installer dir does not exist")
    end
    
    -- TODO: set ISO_PATH and USB_PATH enviornment variable    
    os.setenv("ISO_PATH", isoDir)
    os.setenv("USB_PATH", usbDir)
	os.setenv("INSTALLERISO_PATH", installerIsoDir)
    
    cmd = installerIsoDir.."/"..script   
    print(cmd)
    
    ignore_mcd_event = true
    local ret_code = os.execute(cmd)/256
    
    -- This will return only in case of an error, 
    -- in normal case the script will run and then will do a reset
        
    ignore_mcd_event  = false
    if (ret_code ~= 0) then             
        print (string.format("ret_code from external start script %d",ret_code))         
    end 
    
    return {}
end 
