--module(..., package.seeall)

--[[
This script is run during normal operation.

- Wait until update media is inserted
- Authenticate update image
- Load manifest
- Prompt user for update
- Begin update
]]

local loader    = require "cmc.service.swdlMediaDetect.loader"
local onoff     = require "onoff"
local io        = io
local os        = os
local service   = require("service")

local stored_manifest   = nil 
local updateService     = nil
local service           = require "service"
local serviceName       = "com.harman.service.SoftwareUpdate"
local methods           = {}
local g_version_file    = "/etc/version.txt"  --version file on target
local g_status          = {}    -- Software Update Status variable.
local lfs               = require "lfs"
local autoupdateFile    = "/fs/usb0/autoupdate"
 -- SW Version file at etfs
local g_old_sw_version_file    = "/fs/etfs/version.txt" 
local g_old_sw_version = nil
-- Refernce version for comparison for factory cleanup.
local g_factoryResetRefernceVersion = "15.00.00"
--------------------------------------------------------------------------------
-- Get file size
--------------------------------------------------------------------------------
local function fileSize(filePath)
   -- 2147483647 is int32 max value
   local fileSizeTmp = "/tmp/fileSizeTmp"..tostring(math.random(2147483647))..".txt"
   os.execute("ls -l "..filePath.." | awk '{ print $5}' > "..fileSizeTmp)
   local file = io.open(fileSizeTmp, "r")
   local file_size=file:read()
   file:close()
   os.execute("rm "..fileSizeTmp)
   file_size=tonumber(file_size)
   print("[SWDL] filePath=", filePath, "fileSizeTmp=", fileSizeTmp, "file_size=", file_size)
   return file_size
end

--------------------------------------------------------------------------------
-- Get folder size
--------------------------------------------------------------------------------
local function folderSize(folderPath)
   -- 2147483647 is int32 max value
   local folderSizeTmp = "/tmp/folderSizeTmp"..tostring(math.random(2147483647))..".txt"
   os.execute("du -sp "..folderPath.." | awk '{ print $1}' > "..folderSizeTmp)
   local file = io.open(folderSizeTmp, "r")
   local folder_size=file:read()
   file:close()
   os.execute("rm "..folderSizeTmp)
   folder_size=tonumber(folder_size)
   print("[SWDL] folderPath=", folderPath, "folderSizeTmp=", folderSizeTmp, "folder_size=", folder_size)
   return folder_size
end

--------------------------------------------------------------------------------
-- Get estimated time in minutes for SWDL
--------------------------------------------------------------------------------
local function swdlEstimateTime(update)
   local isoFileSize = fileSize("/fs/usb0/swdl.upd")
   local swdlSize = 0
   
   if (update == "DealerUpdate") then
      local dealerUpdateFolderSize = folderSize("/fs/usb0/nav/")
      swdlSize = isoFileSize + dealerUpdateFolderSize
   else --"NormalUpdate"
      swdlSize = isoFileSize
   end
   
   -- 100MB takes 1min, 20min is a tolerance value for SWDL to accomadate ISO validation,..etc(all items other than other than ISO copy)
   local estTime = math.ceil( ( (swdlSize / ((1024*1024)*(100)))*1 ) + 20 )
   print("[SWDL] swdlSize=", swdlSize, "estTime=", estTime)
   return estTime
end

--------------------------------------------------------------------------------
-- Initialze status, everything other than the current version
--------------------------------------------------------------------------------
function resetStatus()
   g_status.state       = "not ready"
   g_status.errorMsg    = nil
   g_status.toVersion   = nil
   g_status.moduleType  = "sw"
   g_status.moduleName  = "application"
   g_status.swdlEstTime = 80
end

--------------------------------------------------------------------------------
-- emit status
--------------------------------------------------------------------------------
function emitStatus(state)
   g_status.state = state or g_status.state
   return service.emit( updateService, "status", g_status )
end

--------------------------------------------------------------------------------
-- emit error 
--------------------------------------------------------------------------------
function emitError(errorMsg)
   g_status.state = "error"
   g_status.errorMsg = errorMsg or "error"
   return service.emit( updateService, "status", g_status )
end

--------------------------------------------------------------------------------
-- unRegister Software installer service 
--------------------------------------------------------------------------------
function unRegister()
    print("unRegister softwareInstaller")
    service.unregister(updateService)    
    updateService = nil
end  

--------------------------------------------------------------------------------
-- Start the software update dbus service
--------------------------------------------------------------------------------
function register()
    print("register softwareInstaller")
    updateService = assert(service.register(serviceName, methods))
end  

--------------------------------------------------------------------------------
-- Function to get file present status
-- @filePath The path of the file
--------------------------------------------------------------------------------
local function is_file_exist(filePath)
   -- Check if source file exists
   local cmd
   local file_present = true
   cmd = "ls -l "..filePath
   print(cmd)   
   
   local f = assert (io.popen (cmd, "r")) 
   local lines = f:read("*l") 
   print(lines)
   if (lines == nil) then     
      file_present = false      
   end   
   f:close()
   
   return file_present
end

--------------------------------------------------------------------------------
-- getVersionFromEtfs from version file on etfs 
--------------------------------------------------------------------------------
local function getVersionFromEtfs()
    local temp
    local f = io.open(g_old_sw_version_file, "r")
    if not f then 
      g_old_sw_version = "UNKNOWN"        
    else
        for line in f:lines() do                
            temp =  string.match(line,"^%s*version%s*=%s*(%C*)") 
            if temp then                 
               g_old_sw_version = tostring(temp)                
            break
            end                    
        end 
        f:close() 
    end
end    


--------------------------------------------------------------------------------
-- getCurrentVersion from version file on target 
--------------------------------------------------------------------------------
local function getCurrentVersion(version_file)
    local temp
    local f = io.open(version_file, "r")
    if not f then 
          g_status.fromVersion = "UNKNOWN"        
    else
        for line in f:lines() do                
            temp =  string.match(line,"^%s*version%s*=%s*(%C*)") 
            if temp then                 
                g_status.fromVersion = tostring(temp)                
            break
            end                    
        end 
        f:close() 
    end
end    


--------------------------------------------------------------------------------
-- executeFactoryResetIfRequired from version file on target 
--------------------------------------------------------------------------------
local function executeFactoryResetIfRequired()
   print("executeFactoryResetIfRequired() -------> ")
   if (is_file_exist(g_old_sw_version_file) == true) then 
      -- get the version from version.txt in ETFS and stored in "g_old_sw_version_file"
      getVersionFromEtfs()  
      print("version.txt exist under /fs/etfs/, g_factoryResetRefernceVersion=", g_factoryResetRefernceVersion, "g_old_sw_version=", g_old_sw_version)
      if (g_factoryResetRefernceVersion > g_old_sw_version) then
         print("Software update from Old to New, execute Factory Reset")
         -- The flag is set in MMC0 since the factory reset removes etfs files and at second reset
         -- of factory cleanup.sh, it cannot be verified.
         -- Necessary to mount since by this time the mmc0 is readonly
         os.execute("mount -uw /fs/mmc0")
         os.execute("touch /fs/mmc0/FACTORY_RESET_PERFORMED")
         os.execute("sync")
         -- no need to remove "g_old_sw_version_file" it as factory reset will delete the file any way
         -- perform the factory cleanup
         service.invoke("com.harman.service.platform", "do_factory_reset" )
         os.execute("sync")
         -- As we are going to reset no need to execute any thing further
         os.sleep(100)
      else
         local cmd = "rm "..g_old_sw_version_file
         os.execute(cmd)
			print(cmd)
			os.execute("sync")
      end
   else
      if (is_file_exist("/fs/mmc0/FACTORY_RESET_PERFORMED") == true) then
         -- Necessary to mount since by this time the mmc0 is readonly
         os.execute("mount -uw /fs/mmc0")
         os.execute("rm -r /fs/mmc0/FACTORY_RESET_PERFORMED")
         os.execute("sync")
         os.sleep(1)
         print("Factory Reset is performed already, Reset again to complete the Factory Reset Operation")
         -- perform reset
         onoff.reset()
         -- As we are going to reset no need to execute any thing further
         os.sleep(100)
      end
   end
end


---------------------------------------------------------------------
-- This function is called with the manifest after an update media
-- has been inserted , authenticated, mounted and the loaded the 
-- manifest
---------------------------------------------------------------------
function processManifest(manifest, err, detected)
    if detected == true then -- CR#5323 for displaying the popup
       emitStatus("detected")
       return {}
    end
    if manifest then 
        -- check if manifest is for external install 
        if (manifest.external and manifest.external.start_script) then            
           g_status.swdlEstTime = swdlEstimateTime("DealerUpdate")
           emitStatus(nil)
            -- unRegister service 
            unRegister()
            -- execute external script this will not return             
            loader.executeExternalScript(manifest.external.start_script)
            -- return from here means we are ready to take the control 
            -- register the dbus-servcie 
            register()
            -- This is done just for precaution, cleanup settings             
            onoff.resetUpdateInProgress()
            onoff.setUpdateMode(false)           
        else 
            g_status.swdlEstTime = swdlEstimateTime("NormalUpdate")
            stored_manifest = manifest
            -- read the module type from manifest 
            -- if it exists
            if (manifest.module_type ~= nil) then 
                g_status.moduleName = manifest.module_type
            end    
            -- read the target version file from manifest 
            -- if it exists, else read the default version 
            -- file
            if (manifest.target_version_file ~= nil) then 
                getCurrentVersion(manifest.target_version_file)
            else                
               -- if no version file specified then 
               -- read the version from default file
               getCurrentVersion(g_version_file)
            end
            
            -- emit send HMI signal 
            g_status.toVersion = manifest.version      
			-- check for autoUpdate
			print ("checking for autoUpdate...")
			if ((lfs.attributes(autoupdateFile)) ~= nil) then 
				if (g_status.toVersion == g_status.fromVersion) then
					print ("...autoupdate not allowed...")
					loader.setAutoUpdateStatus(50,5)
				else
					print ("autoupdate kicks in...")
					beginUpdate() 
				end
			end	
			
			emitStatus("ready")                
        end                    
    else   
        -- set flag to detect that an inserted signal is sent by this service
        emitError(err)       
    end    
    
   loader.umountISO()      
   return {}
end            
     
----------------------------------------------------------------------
-- Begin update. This is called after the user confirms 
----------------------------------------------------------------------       
function beginUpdate()        
      onoff.setUpdateMode(true)
      -- set the boot flag which is what expected by software download 
      -- after reset
      onoff.setExpectedIOCBootMode("bolo")
      emitStatus("reset")
      -- reset the system with io controller in bolo mode      
      service.invoke("com.harman.service.onOff", 'requestReset',{ type = "bolo" } )
end
   
   
----------------------------------------------------------------------
-- When USB ejected, 
----------------------------------------------------------------------  
function ejected()     
    onoff.setUpdateMode(false)
    -- send the signal only if there was an inserted signal sent before
    if (g_status.state == "ready") then    
        print(" USB EJECTED")     
        emitStatus("ejected")     
    end    
    stored_manifest = nil
    resetStatus()
end

--------------------------------------------------------------------------------
-- Update Method 
--------------------------------------------------------------------------------
function methods.beginUpdate(params)
   if g_status.state == "ready" then     
      beginUpdate()
   else
      emitError("not ready to begin update.")
   end
end

--------------------------------------------------------------------------------
-- Cancel Update
--------------------------------------------------------------------------------
function methods.cancelUpdate(params)
   if g_status.state == "ready" then
      emitStatus("cancelled")
      resetStatus()
   end
end

--------------------------------------------------------------------------------
-- getStatus 
--------------------------------------------------------------------------------
function methods.getStatus()
    emitStatus(nil)   
end 



--------------------------------------------------------------------------------
-- Set Update Mode to false, set mcd notifications , configure the gpio pins 
--------------------------------------------------------------------------------
register()
resetStatus()
onoff.resetUpdateInProgress()
onoff.setUpdateMode(false)
executeFactoryResetIfRequired()
loader.notifyOnInsert(processManifest)
loader.notifyOnEject(ejected)
 
----------------------------------------------------------------------------------
-- OtaService Handling for FOTA 
----------------------------------------------------------------------------------
local myOtaService    = "com.harman.service.OtaService"

local function onOtaRebootRequest(signal, data)
      onoff.setDiffUpdateMode(true)
      -- set the boot flag which is what expected by software download 
      -- after reset
      onoff.setExpectedIOCBootMode("bolo")
      emitStatus("reset")
      -- reset the system with io controller in bolo mode      
      service.invoke("com.harman.service.onOff", 'requestReset',{ type = "bolo" } )
end

local function handleSierraRetryIfNeeded()
   local sierraError=false
   local attemptNumber=0
   local totalAttemptCount=0
   local content=nil
   local f=nil

   -- Check if sierra_air_card error present
   f = io.open("/fs/mmc3/update/FOTA_COMPLETE_STATUS/FOTA_UPDATE_ERROR_HMI", "r")
   if f then 
      for line in f:lines() do                
         if string.match(line,"sierra_air_card") then                 
            sierraError=true                
            break
         end                    
      end 
      f:close() 
   end
   if (sierraError == false) then
      return
   end
   print("[SWDL] Detected sierra_air_card error")

   -- Get attemptNumber value
   f = io.open("/fs/mmc3/update/FOTA_COMPLETE_STATUS/attemptNumber", "r")
   if f then
      content = f:read("*all")
      attemptNumber=tonumber(content)
      f:close()
   else
      -- We will never reach here
      print("[SWDL] Can't proceed for sierra_air_card update retry as attemptNumber file not present")
      return
   end

   -- Get totalAttemptCount value
   f = io.open("/fs/mmc3/update/FOTA_COMPLETE_STATUS/totalAttemptCount", "r")
   if f then
      content = f:read("*all")
      totalAttemptCount=tonumber(content)
      f:close()
   else
      -- We will never reach here
      print("[SWDL] Can't proceed for sierra_air_card update retry as totalAttemptCount file not present")
      return
   end

   if not (attemptNumber < totalAttemptCount) then
      print("[SWDL] Can't proceed for sierra_air_card update retry as attemptNumber=", attemptNumber, "totalAttemptCount=", totalAttemptCount)
      return
   end
   
   attemptNumber=attemptNumber+1
   local attemptNumberStr=tostring(attemptNumber)
   -- Get attemptNumber value
   f = io.open("/fs/mmc3/update/FOTA_COMPLETE_STATUS/attemptNumber", "w+")
   if f then
      f:write(attemptNumberStr)
      f:close()
   else
      -- We will never reach here
      print("[SWDL] attemptNumber increment was failed")
   end

   -- Remove UpdateAgent result file and do the update again
   os.execute("rm /fs/mmc3/update/ota/rb/results")
   -- Remove FOTA_UPDATE_ERROR_HMI file and retry the update
   os.execute("rm /fs/mmc3/update/FOTA_COMPLETE_STATUS/FOTA_UPDATE_ERROR_HMI")
   -- Retry sierra_air_card update by entering update mode
   print("[SWDL] Entering update mode to retry sierra_air_card update")
   onOtaRebootRequest()
   -- As we are going to reset no need to execute any thing further
   os.sleep(100)
end

----------------------------------------------------------------------------------
-- Invoke all FOTA related functions here 
----------------------------------------------------------------------------------
handleSierraRetryIfNeeded()
service.subscribe(myOtaService, "ota_scomo_reboot_request", onOtaRebootRequest)

-- Once /tmp/SoftwareUpdateInitDone is set then only we should launch smm.exe for syncronization between SoftwareUpdate DBUS and OtaService DBUS
-- This flag should be set only at the end
os.execute("touch /tmp/SoftwareUpdateInitDone")
