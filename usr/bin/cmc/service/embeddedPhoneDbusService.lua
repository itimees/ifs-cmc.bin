--local variables
local service = require "service"
local epcore  = require "epcore"
local json    = require "json"

local embeddedPhoneServiceName = "com.harman.service.EmbeddedPhone"
local framService = "com.harman.service.PersistentKeyValue"

local methods = {}
local embeddedPhoneService
local encode = json.encode
local decode = json.decode

local framAvailableSubscription

-- This flag is needed as old service is not calling resume call to activate held call
local heldCall = false

function emit(signal,params)
   service.emit(embeddedPhoneService, signal, params)
end
dummyRes = {code = 14,
            description = "Default Error"}
            
local function standardResponse(context, replyExpected)
    local response
   	if replyExpected then
    	function response(code, description)
       		service.returnResult(context, {code = code, description = description})
 	  	end
	end
    return response
end

local function standardResponseWithReason(context, replyExpected)
	local response
	if replyExpected then
		function response( code, description,reason )
			service.returnResult ( context, {code = code, description = description ,reason = reason} )
		end
	end
	return response
end
--string split function
function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end
-----------------------------------------------------------------------
-- event handlers

local events = {}

local function processEvent(event, ...)
   local handler = events[event]
   if handler then
      handler(...)
   else
      print("no handler for event", event)
   end
end

function events.embeddedPhoneStatus(status)
   emit("embeddedPhoneStatus", {status = status})
end

function events.callState(callStateInfo)
   emit("callState", {callStateInfo = callStateInfo})
end

function events.incomingCallInfo(number, callId, inband)
   if (inband == nil) then
      emit("incomingCallInfo", {number = number, callId = callId, inband = false})
   else
      emit("incomingCallInfo", {number = number, callId = callId, inband = inband})
   end
end

function events.signalQuality(rssi)
   emit("signalQuality", {rssi = rssi})
end

function events.batteryCharge(level)
   emit("batteryCharge", {level = level})
end

function events.networkRegistrationState(state)
   emit("networkRegistrationState", {state = state})
end

function events.networkOperatorChanged(code, longName, shortName, mode, accTech)
   emit("networkOperatorChanged", {operatorCode = code, operatorLongName = longName,
       operatorShortName = shortName, operatorMode = mode, operatorAccTech = accTech})
end

function events.phoneReset(dump)
   emit("phoneReset", {dump = dump})
end

function events.updateProgress(percentage)
   emit("updateProgress", {percentage = percentage})
end

function events.hwState(hwState)
   emit("hwState", {hwState = hwState})
end

function events.hwState(buffer,receivedMsg)
   emit("ecallInfo", {buffer = buffer,receivedMsg = receivedMsg})
end

function events.wendReason(reason,serviceNumber)
   emit("wendReason", {reason = reason,serviceNumber = serviceNumber})
end

function events.wcntReason(reason)
   emit("wcntReason", {reason = reason})
end

function events.antennaStatus(antenna,status,adcReading)
   emit("antennaStatus", {antenna = antenna,status = status,adcReading = adcReading})
end

function events.tempState(state)
   emit("tempState", {state = state})
end
function events.pmicTempChanged(temperature)
   emit("pmicTempChanged", {temperature = temperature})
end
function events.paTempChanged(temperature)
   emit("paTempChanged", {temperature = temperature})
end

function events.newSms(queryId,srcPort,destPort)
   emit("newSms", {queryId = queryId,destPort = destPort})
end

function events.deviceServiceState(event,data)
   emit("deviceServiceState", {event = event,data = data})
end


-----------------------------------------------------------------------
-- persistency interface

local function readPersistence(key)
   local val = 0
   local request = {}
   request.key = key;
   local persistentData, errStr = service.invoke(framService,"read", request)
   if ((persistentData ~= nil) and (errStr == nil)) then
      if(key == "BtAutoConnectList") then
         val = decode(persistentData.res)
      elseif (key == "BtPairedDeviceList") then
         val = decode(persistentData.res)
      else
         val = persistentData.res      
      end
   else
      print("Error Reading Persistent data.  Error is: " ..errStr)
   end   
   return val
end

local function writePersistence(key,value)
   local val
   if (key == "BtAutoConnectList") then
      val = encode(value)
   elseif (key == "BtPairedDeviceList") then
      val = encode(value)
   else
      val = value
   end
   
   local result = service.invoke(framService,"write", {[key] = val})
end
-----------------------------------------------------------------------

local config_filepath = "/etc/wicome/embPhoneService.cfg"

local function start()
   embeddedPhoneService = assert(service.register(embeddedPhoneServiceName, methods))
   epcore.registerEventCallBack(processEvent)
   epcore.registerFramCallBack(readPersistence,writePersistence)
   epcore.startup(config_filepath)
end

-- dbus methods 
function methods.getProperties(params,context,replyExpected)
	if type(params.props) == "table" then
  		return epcore.getProperties(params.props) 
  	else
  		return dummyRes
  	end
end
function methods.setProperties(params,context,replyExpected)
   local ret = epcore.setProperties(params,context,replyExpected)
   if ret == -1 then
      return dummyRes
   end
end

function methods.setEmbeddedPhoneStatus(params,context,replyExpected)
	local response = standardResponse(context, replyExpected)
	if params.enable == true then
	    if not epcore.setEmbeddedPhoneOn(response) then
      	 return dummyRes
   		end
    else
	   if not epcore.setEmbeddedPhoneOff(response) then
       	return dummyRes
   	   end
	end
end


function methods.dial(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, reason, callId)
         service.returnResult(context, {code = code, description = description, reason = reason, callId = callId})
      end
   end
   if not epcore.dial(response, params.number) then
      return dummyRes
   end
end
function methods.ecallDial(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, reason, callId)
         service.returnResult(context, {code = code, description = description,reason = reason, callId = callId})
      end
   end
   if not epcore.ecallDial(response, params.address,params.callType,params.vehicleType) then
      return dummyRes
   end
end
function methods.writeTtyStr(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, respCode)
         service.returnResult(context, {code = code, description = description, respCode = respCode})
      end
   end
   if not epcore.writeTtyStr(response, params.ttyString) then
      return dummyRes
   end
end

function methods.reDial(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, reason, callId)
         service.returnResult(context, {code = code, description = description, reason = reason, callId = callId})
      end
   end
   if not epcore.reDial(response) then
      return dummyRes
   end
end


function methods.acceptIncomingCall(params,context,replyExpected)
   local response = standardResponseWithReason( context,replyExpected )
   if not epcore.acceptIncomingCall(response) then
      return dummyRes
   end
end

function methods.endCall(params,context,replyExpected)
   local response = standardResponseWithReason( context,replyExpected )
   if not epcore.endCall(response,params.callId) then
   	  return dummyRes
   end
end
function methods.endActiveCall(params,context,replyExpected)
   local response = standardResponseWithReason( context,replyExpected )
   if not epcore.endActiveCall(response) then
   	  return dummyRes
   end
end

function methods.rejectIncomingCall(params,context,replyExpected)
  local response = standardResponseWithReason( context,replyExpected )
  if not epcore.rejectIncomingCall(response) then
      return dummyRes
   end
end
function methods.restoreFactorySettings(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.restoreFactorySettings(response) then
      return dummyRes
   end
end
function methods.getCallState(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, callStateInfo)
         service.returnResult(context, {code = code, description = description,callStateInfo = callStateInfo})
      end
   end
   if not epcore.getCallList(response) then
      return dummyRes
   end
end
function methods.getAntennaStatus(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, status)
         service.returnResult(context, {code = code, description = description,status = status})
      end
   end
   if not epcore.getAntennaStatus(response,params.antenna) then
      return dummyRes
   end
end
function methods.getTemperature(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, temperature,maxTemperature)
         service.returnResult(context, {code = code, description = description, temperature = temperature,maxTemperature = maxTemperature})
      end
   end
   if not epcore.getTemperature(response) then
      return dummyRes
   end
end
function methods.getPaTemperature(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, temperature,maxTemperature)
         service.returnResult(context, {code = code, description = description, temperature = temperature,maxTemperature = maxTemperature})
      end
   end
   if not epcore.getPaTemperature(response) then
      return dummyRes
   end
end
function methods.getWENDReason(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, wendReason,serviceNumber)
         service.returnResult(context, {code = code, description = description, wendReason = wendReason,serviceNumber = serviceNumber})
      end
   end
   if not epcore.getWENDReason(response) then
      return dummyRes
   end
end
function methods.getEmergencyNumbers(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, count,numberList)
         service.returnResult(context, {code = code, description = description, count = count,numberList = numberList })
      end
   end
   if not epcore.getEmergencyNumbers(response,params.maxCount) then
      return dummyRes
   end
end
function methods.startUpdate(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.startUpdate(response,params.dataSize) then
      return dummyRes
   end
end
function methods.transferUpdateData(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.transferUpdateData(response,params.data) then
      return dummyRes
   end
end
function methods.finishUpdate(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.finishUpdate(response) then
      return dummyRes
   end
end
function methods.resetPhone(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.resetPhone(response) then
      return dummyRes
   end
end
function methods.getSubscriberNumber(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, count, numberList, numberTypeList,serviceList)
         service.returnResult(context, {code = code, description = description, count = count,numberList=numberList, numberTypeList=numberTypeList, serviceList=serviceList })
      end
   end
  if not epcore.getSubscriberNumber(response,params.maxCount) then
      return dummyRes
   end
end
function methods.getSwRevision(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, revision)
		 local swApplVer = ""
		 local swBootVer = ""
		 local res = split(revision,";")
		 for k,v in ipairs(res) do
			if string.find(v,"APPL") ~= nil then
				swApplVer = string.sub(v,15,-20)
			elseif string.find(v,"BOOT") ~= nil then
		 		swBootVer = string.sub(v,15,-20)	
			end
		 end
         service.returnResult(context, {code = code, description = description, swApplVer = swApplVer,swBootVer = swBootVer})
      end
   end
   if not epcore.getSwRevision(response) then
      return dummyRes
   end
end
function methods.getMEID(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, data)
      local res
      local imei
      local esn
      	 res = split(data,",")
      	 imei = string.sub(res[1],5)
      	 esn = string.sub(res[2],3)
         service.returnResult(context, {code = code, description = description, imei = imei,esn = esn})
      end
   end
   if not epcore.getMEID(response) then
      return dummyRes
   end
end
function methods.getIMSI(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, imsi)
         service.returnResult(context, {code = code, description = description, imsi = imsi})
      end
   end
   if not epcore.getIMSI(response) then
      return dummyRes
   end
end
function methods.getDevTime(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, devTime)
         service.returnResult(context, {code = code, description = description, devTime = devTime})
      end
   end
   if not epcore.getDevTime(response) then
      return dummyRes
   end
end
function methods.getDevStatus(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, devStatus)
         service.returnResult(context, {code = code, description = description, devStatus = devStatus})
      end
   end
   if not epcore.getDevStatus(response) then
      return dummyRes
   end
end
function methods.getServingSystem(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, bandClass, band, baseStationProtocolRev, sid, rfChNumber)
         service.returnResult(context, {code = code, description = description, bandClass = bandClass, band = band, baseStationProtocolRev = baseStationProtocolRev, sid = sid, rfChNumber = rfChNumber})
      end
   end
   if not epcore.getServingSystem(response) then
      return dummyRes
   end
end
function methods.getModel(params,context,replyExpected)
	local response
   if replyExpected then
      function response(code, description, res)
		local ares = split(res,";")	
		local model = ares[1]
		local hwVersion = ares[2]
        service.returnResult(context, {code = code, description = description, model = model,hwVersion = hwVersion})
      end
   end
   if not epcore.getModel(response) then
      return dummyRes
   end
end
function methods.getSerialNumber(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, serialNumber)
         service.returnResult(context, {code = code, description = description, serialNumber = serialNumber})
      end
   end
   if not epcore.getSerialNumber(response) then
      return dummyRes
   end
end
function methods.sendDtmf(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.sendDtmf(response,params.sequence) then
      return dummyRes
   end
end
function methods.sendSms(params,context,replyExpected)
  local response = standardResponseWithReason( context,replyExpected )
  if not epcore.sendSms(response,params.number,params.smsBody,params.priority) then
      return dummyRes
   end
end
function methods.getNewSms(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, smsList)
         service.returnResult(context, {code = code, description = description, smsList = smsList })
      end
   end
  if not epcore.getNewSms(response,params.queryId,params.storageType) then
      return dummyRes
   end
end
function methods.getPendingSms(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, smsList)
         service.returnResult(context, {code = code, description = description, smsList = smsList })
      end
   end
  if not epcore.getPendingSms(response,params.appId,params.destPort) then
      return dummyRes
   end
end
function methods.deleteSms(params,context,replyExpected)
  local response = standardResponseWithReason( context,replyExpected )
  if not epcore.deleteSms(response,params.status,params.queryId) then
      return dummyRes
   end
end
function methods.registerSmsPort(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.registerSmsPort(response,params.appId,params.destPort) then
      return dummyRes
   end
end
function methods.unregisterSmsPort(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.unregisterSmsPort(response,params.appId,params.destPort) then
      return dummyRes
   end
end
function methods.openDataConnection(params,context,replyExpected)
  local response = standardResponseWithReason( context,replyExpected )
  if not epcore.openDataConnection(response,params.number,params.name,params.username,params.password ) then
      return dummyRes
   end
end
function methods.closeDataConnection(params,context,replyExpected)
  local response = standardResponseWithReason( context,replyExpected )
  if not epcore.closeDataConnection(response,params.name) then
      return dummyRes
   end
end
function methods.getProfileInformation(params,context,replyExpected)
   local response
   local res
   local subRes
   local pInfo = {
   					nai = ""
   					,HA_IPAdress_home = ""
   					,HA_IPAdress_primary = ""
   					,HA_IPAdress_secondary = ""
   					,MN_AAA_SharedSecretSPI = ""
   					,MN_HA_SharedSecretSPI = ""
   					,MN_AAA_SharedSecretKey = ""
   					,MN_HA_SharedSecretKey = ""
   					}
   if replyExpected then
      function response(code, description, profileInfo)
      	 res = split(profileInfo,";")
      	 for _,v in ipairs(res) do
      	    subRes = split(v,":")
      	    if subRes[1] == "NAI" then
      	    	pInfo.nai = subRes[2]
      	    elseif subRes[1] == "Home Addr" then
      	    	pInfo.HA_IPAdress_home = subRes[2]
      	    elseif subRes[1] == "Primary HA" then
      	    	pInfo.HA_IPAdress_primary = subRes[2]
      	    elseif subRes[1] == "Secondary HA" then
      	    	pInfo.HA_IPAdress_secondary = subRes[2]
      	    elseif subRes[1] == "MN-AAA SPI" then
      	    	pInfo.MN_AAA_SharedSecretSPI = subRes[2]
      	    elseif subRes[1] == "MN-HA SPI" then
      	    	pInfo.MN_HA_SharedSecretSPI = subRes[2]
      	    elseif subRes[1] == "MN-AAA SS" then
      	    	pInfo.MN_AAA_SharedSecretKey = subRes[2]
      	    elseif subRes[1] == "MN-HA SS" then
      	    	pInfo.MN_HA_SharedSecretKey = subRes[2]
      	    else 
      	    end
      	 end
         service.returnResult(context, {code = code, description = description, profileInfo = pInfo})
      end
   end
   if not epcore.getProfileInformation(response,params.profileNumber) then
      return dummyRes
   end
end
function methods.selectProfile(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.selectProfile(response,params.profileNumber) then
      return dummyRes
   end
end
function methods.getSIDNID(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, count, indexList, sidList,nidList)
         service.returnResult(context, {code = code, description = description, count = count,indexList=indexList,sidList=sidList,nidList=nidList})
      end
   end
   if not epcore.getSIDNID(response,params.spc,params.maxCount) then
      return dummyRes
   end
end
function methods.getPrlVersion(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, prlVersion)
         service.returnResult(context, {code = code, description = description, prlVersion = prlVersion})
      end
   end
   if not epcore.getPrlVersion(response,params.maxCount) then
      return dummyRes
   end
end
function methods.setProvisioning(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setProvisioning(response,params.spc
                                        ,params.imsi
                                        ,params.mdn
                                        ,params.nai
                                        ,params.MN_AAA_SharedSecretKey
                                        ,params.MN_AAA_SharedSecretSPI
                                        ,params.MN_HA_SharedSecretKey
                                        ,params.MN_HA_SharedSecretSPI
                                        ,params.HA_IPAdress_primary
                                        ,params.HA_IPAdress_secondary
                                        ,params.HA_IPAdress_home
                                        ,params.rescan_interval
                                        ,params.diagnose_port
                                        ) then
      return dummyRes
   end
end
function methods.getUsbPorts(params,context,replyExpected)
   local response = {code = 0
   					,description = "success"
   					,ports = {}}
    local f = io.popen("ls /dev/swiusb*") -- runs command
    local l = f:read("*a") -- read output of command
    f:close()
	local res = split(l,"\n")
	for k,v in ipairs(res) do
		response.ports[k] = res[k]
	end
    return response
end
function methods.debugAT(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.debugAT(response,params.atString) then
      return dummyRes
   end
end
function methods.exitEmergencyMode(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.debugAT(response,"AT+WSOS=0\r") then
      return dummyRes
   end
end

function methods.getMdn(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, mdn)
         service.returnResult(context, {code = code, description = description,mdn = mdn})
      end
   end
   if not epcore.getMdn(response) then
      return dummyRes
   end
end

function methods.getPri(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, pri)
         service.returnResult(context, {code = code, description = description,pri = pri})
      end
   end
   if not epcore.getPri(response) then
      return dummyRes
   end
end

function methods.getNai(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, nai)
         service.returnResult(context, {code = code, description = description,nai = nai})
      end
   end
   if not epcore.getNai(response) then
      return dummyRes
   end
end
function methods.getReverseTunneling(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, reverseTunneling,commit)
         service.returnResult(context, {code = code, description = description,reverseTunneling = reverseTunneling,commit=commit})
      end
   end
   if not epcore.getReverseTunneling(response) then
      return dummyRes
   end
end
function methods.getHspi(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, hspi,commit)
         service.returnResult(context, {code = code, description = description,hspi = hspi,commit=commit})
      end
   end
   if not epcore.getHspi(response) then
      return dummyRes
   end
end
function methods.getAspi(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, aspi,commit)
         service.returnResult(context, {code = code, description = description,aspi = aspi,commit=commit})
      end
   end
   if not epcore.getAspi(response) then
      return dummyRes
   end
end
function methods.getMobileStationIp(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, ip)
         service.returnResult(context, {code = code, description = description,ip = ip})
      end
   end
   if not epcore.getMobileStationIp(response) then
      return dummyRes
   end
end

function methods.getPrimaryHaAddress(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, address,commit)
         service.returnResult(context, {code = code, description = description,address = address,commit=commit})
      end
   end
   if not epcore.getPrimaryHaAddress(response) then
      return dummyRes
   end
end
function methods.getSecondaryHaAddress(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, address,commit)
         service.returnResult(context, {code = code, description = description,address = address,commit=commit})
      end
   end
   if not epcore.getSecondaryHaAddress(response) then
      return dummyRes
   end
end
function methods.getAccessOverloadClass(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, aoc)
         service.returnResult(context, {code = code, description = description,aoc = aoc})
      end
   end
   if not epcore.getAccessOverloadClass(response) then
      return dummyRes
   end
end
function methods.getSlotCycleIndex(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, sci)
         service.returnResult(context, {code = code, description = description,sci = sci})
      end
   end
   if not epcore.getSlotCycleIndex(response) then
      return dummyRes
   end
end
function methods.getMnHaSharedSecrets(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, mnhaCode)
         service.returnResult(context, {code = code, description = description,mnhaCode = mnhaCode})
      end
   end
   if not epcore.getMnHaSharedSecrets(response) then
      return dummyRes
   end
end
function methods.getMnAaaSharedSecrets(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, mnaaaCode)
         service.returnResult(context, {code = code, description = description,mnaaaCode = mnaaaCode})
      end
   end
   if not epcore.getMnAaaSharedSecrets(response) then
      return dummyRes
   end
end
function methods.getAccessPointName(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, apn,dssAction)
         service.returnResult(context, {code = code, description = description,apn = apn,dssAction=dssAction})
      end
   end
   if not epcore.getAccessPointName(response) then
      return dummyRes
   end
end
function methods.getCdmaSystemStatus(params,context,replyExpected)
   local response
   if replyExpected then
      function response(code, description, status)
		 local res1
		 local index = 1
		 local currentMenu
		 local array = {oneXEngineering = {},
						evdoEngineering = {},
						bluetooth = {},
						memoryUtilization = {},
						dataStatus = {},
						configuration = {},
						}
			if status then
				res1 = split(status,";")
				for _,v in ipairs(res1) do
					if string.find(v,":") then
						if currentMenu == "1x Engineering" then
							array.oneXEngineering[index] = v
							index = index + 1
						elseif currentMenu == "EVDO Engineering" then
							array.evdoEngineering[index] = v
							index = index + 1
						elseif currentMenu == "Configuration " then
							array.configuration[index] = v
							index = index + 1
						elseif currentMenu == "Bluetooth" then
							array.bluetooth[index] = v
							index = index + 1
						elseif currentMenu == "Data Status" then
							array.dataStatus[index] = v
							index = index + 1
						elseif currentMenu == "Memory Utilization" then
							array.memoryUtilization[index] = v
							index = index + 1
						else
						end
					elseif string.find(v,",") then
							if currentMenu == "1x Engineering" then
								array.oneXEngineering[index-1] = array.oneXEngineering[index-1]..v
							end
					else
							currentMenu = v
							index = 1
					end
				end
			end
			
         service.returnResult(context, {code = code, description = description,status = array})
      end
   end
   if not epcore.getCdmaSystemStatus(response) then
      return dummyRes
   end
end
function methods.setImsi(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setImsi(response,params.imsi) then
      return dummyRes
   end
end
function methods.setMdn(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setMdn(response,params.mdn) then
      return dummyRes
   end
end
function methods.setNai(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setNai(response,params.nai,params.commit) then
      return dummyRes
   end
end
function methods.setReverseTunneling(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setReverseTunneling(response,params.reverseTunneling,params.commit) then
      return dummyRes
   end
end
function methods.setHspi(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setHspi(response,params.hspi,params.commit) then
      return dummyRes
   end
end
function methods.setAspi(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setAspi(response,params.aspi,params.commit) then
      return dummyRes
   end
end
function methods.setPrimaryHaAddress(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setPrimaryHaAddress(response,params.address,params.commit) then
      return dummyRes
   end
end
function methods.setSecondaryHaAddress(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setSecondaryHaAddress(response,params.address,params.commit) then
      return dummyRes
   end
end
function methods.setAccessOverloadClass(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setAccessOverloadClass(response,params.aoc) then
      return dummyRes
   end
end
function methods.setSlotCycleIndex(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setSlotCycleIndex(response,params.sci) then
      return dummyRes
   end
end
function methods.setMnHaSharedSecrets(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setMnHaSharedSecrets(response,params.mnhaCode,params.commit) then
      return dummyRes
   end
end
function methods.setMnAaaSharedSecrets(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setMnAaaSharedSecrets(response,params.mnaaaCode,params.commit) then
      return dummyRes
   end
end
function methods.setSpc(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setSpc(response,params.spc) then
      return dummyRes
   end
end
function methods.setRtn(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setRtn(response,params.spc) then
      return dummyRes
   end
end
function methods.commitChanges(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.commitChanges(response,params.commit) then
      return dummyRes
   end
end
function methods.setAccessPointName(params,context,replyExpected)
  local response = standardResponse( context,replyExpected )
  if not epcore.setAccessPointName(response,params.apn,params.username,params.password) then
      return dummyRes
   end
end

-------------------------------------------------------------------------------
-- Called When the FRAM Service is Available
-------------------------------------------------------------------------------
local function onFramServiceAvailable(newName, oldOwner, newOwner)
   if framAvailableSubscription then
	 service.unsubscribe(framAvailableSubscription)
	 framAvailableSubscription = nil
   end

	 start()
end

-------------------------------------------------------------------------------
-- Initialization
-------------------------------------------------------------------------------
local function init()
	framAvailableSubscription = assert(service.subscribeOwnerChanged(framService, onFramServiceAvailable))
	if service.nameHasOwner(framService) then
	  onFramServiceAvailable("x", "y", "z") -- need to change xyz if we check the args
	end
end

init()