#!/bin/sh

######## BT script ###########  
#set -x

B_LOG=/tmp/BT.log
SH_PATH=`dirname $0`
#BT_BIN_PATH=${SH_PATH}/BTutil/arm/o-le-v7
BT_BIN_PATH=/usr/bin
RESULT="FAIL"
#BT_IO=/tmp/btio_set
BT_PORT_DEV=/dev/ser2
BT_ID_STORE=/dev/mmap/BT_ID
BT_HW_RESET=/dev/gpio/BtReset
BT_MATCH_ID=0
BT_ID_BOGUS=00025B00A5A5

if [ -e ${B_LOG} ] 
then
	rm ${B_LOG}
fi

echo "********************************************************************  "  >>  ${B_LOG}
echo "                        Blue tooth test                            "  >>  ${B_LOG}
echo "********************************************************************  "  >>  ${B_LOG}

#if [ -f $BT_IO ] 
#then
#	echo "BtReset GPIO pin already set"  >> ${B_LOG}	
#else
#	#Set GPIO High 
#	echo "Toggling the BtReset GPIO pin " >> ${B_LOG}
#	###${TEB_MNT_FLDR}/BlueTooth/io -a 0x49052034 -o 0x9FFFFFFF >> ${B_LOG}
#	###${TEB_MNT_FLDR}/BlueTooth/io -a 0x49052090 -o 0x40000000 >> ${B_LOG}
#	###${TEB_MNT_FLDR}/BlueTooth/io -a 0x49052094 -o 0x40000000 >> ${B_LOG}
#	echo -n 1 > /dev/gpio/BtReset >> ${B_LOG}
#	echo -n 0 > /dev/gpio/BtReset >> ${B_LOG}
#	echo -n 1 > /dev/gpio/BtReset >> ${B_LOG}
#	
#	touch $BT_IO
#fi

echo "Executing  BT_test...\n"  >>  ${B_LOG}
while getopts isStf:wX:p:Z: opt $* > /dev/null
do
    case "$opt" in
    
            i)  echo "Initializing BT Chip, ID from FRAM, reading ID...\n" | tee -a ${B_LOG}
				echo "Toggling the BtReset GPIO pin " >> ${B_LOG}
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
				echo -n 0 > $BT_HW_RESET
				sleep 0.1
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
            	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -d$BT_ID_STORE -wi -F /test/BlueTooth/Toyota_BC06_PSKeys_incl_PowerTable_and_Patches.txt -vywr >> ${B_LOG}
            	;;
            f)  echo "Initializing BT Chip, bogus ID, reading ID...\n" | tee -a ${B_LOG}
				BT_ID_BOGUS=${OPTARG}
				echo "Toggling the BtReset GPIO pin " >> ${B_LOG}
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
				echo -n 0 > $BT_HW_RESET
				sleep 0.1
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
            	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -wi -F /test/BlueTooth/Toyota_BC06_PSKeys_incl_PowerTable_and_Patches.txt -v -z$BT_ID_BOGUS -wr >> ${B_LOG}
            	;;				
		    w)  echo "Performing Warm reset on chip\n" | tee -a ${B_LOG}
		    	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -w >> ${B_LOG}
		    	;;
            X)  echo "Initializing BT Inquiry.." | tee -a ${B_LOG}
            	BT_MATCH_ID=${OPTARG}            	
            	#echo $BT_MATCH_ID 
            	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -I${BT_MATCH_ID} >> ${B_LOG}
            	;;
            p)  echo "BT Pairing..\n" | tee -a ${B_LOG}
            	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV $*  | tee -a ${B_LOG}
            	;;
            Z)  echo "Initializing BT Chip and testing inquiry...\n" | tee -a ${B_LOG}
				BT_MATCH_ID=${OPTARG}
				echo "Toggling the BtReset GPIO pin " >> ${B_LOG}
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
				echo -n 0 > $BT_HW_RESET
				sleep 0.1
				echo -n 1 > $BT_HW_RESET
				sleep 0.1
            	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -d$BT_ID_STORE -wi -F /test/BlueTooth/Toyota_BC06_PSKeys_incl_PowerTable_and_Patches.txt -ywr -I${BT_MATCH_ID}>> ${B_LOG}
            	;;
		    s)  echo "BT chip to discoverable state, omap in PCM loopback mode...\n" | tee -a ${B_LOG}
				mictest -a1:0 -b1:0 & >> ${B_LOG}
		    	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -s >> ${B_LOG}
		    	;;
			S)	echo "BT chip warm reset, omap PCM loopback mode cancel...\n" | tee -a ${B_LOG}
				MIC_PID=`pidin a | grep mictest | grep -v grep | awk '{print $1}'`
				if [ -n "$MIC_PID" ]
				then
					echo "\nSlaying mictest process"  >>  ${B_LOG}
					slay ${MIC_PID}
				fi
		    	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -w >> ${B_LOG}
				;;
			t)  echo "BT chip to device under test mode..\n" | tee -a ${B_LOG}
		    	${BT_BIN_PATH}/bt_test -a$BT_PORT_DEV -st >> ${B_LOG}
		    	;;
    esac
done

op=$?
#echo $op
if test op -eq 0
then
	RESULT="PASS"
fi

echo "BlueTooth test:#### TEST ${RESULT} ##### "  >>  ${B_LOG}
if [ -e /tmp/tebuut.conf ] 
then
	. /tmp/tebuut.conf
	cat ${B_LOG} >> ${TEB_LOGNAME}
fi

echo "${RESULT}"
exit 0
