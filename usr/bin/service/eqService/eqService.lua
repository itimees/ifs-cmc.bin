-- eqSvc

local service = require "service"
local eq = require "eqToolSvc"
local json     = require "json"

busName = "com.harman.service.EqToolSvc"
objPath = "/com/harman/service/EqToolSvc"
local myService = nil
methods = {}
callback = {}

---------------------------
-- LOCAL VARIABLES
---------------------------
----------------------------------------------------------
-- properties
properties =
{
   verbosity = 0,
   cabinEq = "99",
   selectionBits = "0",
   modelYear = "99"
}

-- table of property modes.
propmode =
{
   verbosity = "rw"
}

function reloadEq (msg)
	print("LUA - reloadEq")
	service.invoke("com.harman.service.AudioCtrlSvc", "reloadEQ", {})
end


function getActiveFilename ()
print("LUA - getActiveFilename")
   result = service.invoke("com.harman.service.AudioCtrlSvc", "getProperties", {props={"currentEq"}})
   if result and result.currentEq then
		print(result.currentEq)
		return result.currentEq
   else
		print ("ERROR: AudioCtrlSvc did not provide an eq filename")
		return ("ERROR: AudioCtrlSvc did not provide an eq filename")
   end
end

function getSlotFilename (slot)
print("LUA - getSlotFilename")
	if (slot == 0) then
	result = getActiveFilename()
		return result
	else
		return 0
	end
end

function getChimeArray()
	result = service.invoke("com.harman.service.AudioCtrlSvc", "getChimeArray", {})
	if (result ~= nil) then
		if (result.chimeArray ~= nil) then
			return result.chimeArray
		end
	end
    print("LUA - getChimeArray: I don't get a chime_array...")
end

function writeConfigFile(chimeArray)
	print("LUA - writeConfigFile");
	if type(chimeArray) == "table" then
		service.invoke("com.harman.service.AudioCtrlSvc", "setChimeConfig", chimeArray)
	else
	  print("LUA - writeConfigFile: chime_array is not qualified to send over to audioCtrlSvc")
	end
end

--////////////////////////////////////
--// API v1.1  METHODS
--////////////////////////////////////

--- Return the value of some properties
-- @param params.props: an array of property names
-- @return an object with name/value pairs
function methods.getProperties(params, context)
   assert(params.props and params.props[1], "missing or invalid argument 'props'")
   local props = params.props or {}
   local result = {}
   for _,name in ipairs(props) do
      if properties[name] == nil then
         return propertyError(name, context)
      else
         result[name] = properties[name]
      end
   end
   return result
end

function methods.getAllProperties(params)
   refreshProperties()
   local result = {}
   for name,value in pairs(properties) do
      result[name] = {value = value, mode = propmode[name] or "r"}
   end
   return result
end

function methods.setProperties(params, context)
   local props = params.props or {}
   local result = {}

   for name,value in pairs(props) do
      -- only allow setting of writable properties
      if propmode[name]:find("w") then
        properties[name] = value
			emitProperty(name)
			result[name] = properties[name]
			return result
      else
         print("LUA - setProperties, but prop mode not writable: name = ", name, " value = ", value)
         result = failed or {}
         return result
      end
   end
end

function emitProperty(propertyName)
   if (properties.verbosity) then
      print (string.format("eqService.lua: emitProperty %s", propertyName))
   end

   service.emit(myService, propertyName, {value = properties[propertyName]})
end

-------------------------------------------------------
--  Start script portion...
-------------------------------------------------------

-- default params (may be overridden by actual cmd-line params)
properties["-i"] = "/dev/mcd/SER_ATTACH"--"/dev/mcd/SERUSB"
properties["-e"] = "/dev/mcd/SER_DETACH"--"/dev/mcd/EJECTED"
properties["-b"] = "/fs/mmc0/eq"--"/mnt/persistence/eq"
properties["slot"] = 1

-- Parse Command Line Options --
cnt = 1
for cnt = 1, #arg, 2 do
   properties[ arg[cnt] ] = arg[cnt+1]
end

-- if verbosity was specified on cmd-line, this converts
-- it to a number; if it was not specified, no-effect
properties.verbosity = tonumber(properties.verbosity)

if (properties.verbosity > 0) then
   for name,value in pairs(properties) do
      print("eqService.lua: properties["..name.."] = ", value)
   end
end

-- Initialize the tuner --
eq.init(properties)
eq.start(reloadEq)
myService = service.register(busName, methods)
