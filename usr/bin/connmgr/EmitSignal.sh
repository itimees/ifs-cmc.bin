#!/bin/sh
#-------------------------------------
#
# Simple helper script to create a timer event over connmgr
#
#
# Arguments: (v1.0)
# -----------------
# $1 = GUID of device
# $2 = Signal name
#-------------------------------------

dbus-send --type=method_call --dest=com.harman.service.ConnMgr /com/harman/service/ConnMgr com.harman.ServiceIpc.Invoke string:emitProxy string:"{\"guid\":$1, \"name\":\"$2\"}"
