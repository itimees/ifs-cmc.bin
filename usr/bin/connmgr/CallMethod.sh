#!/bin/sh
#-------------------------------------
#
# Simple helper script to call a method within the connection mgr
#
# Arguments: (v1.0)
# -----------------
# arg[1] = $1 = Name of method
# arg[2] = $2 = Data
#-------------------------------------
#echo "arg[1] = Method = $1"
#echo "arg[2] = Data   = $2"
dbus-send --type=method_call --dest=com.harman.service.ConnMgr /com/harman/service/ConnMgr com.harman.ServiceIpc.Invoke string:$1 string:$2

