#!/bin/sh
echo "ifwatchd: interface ${1} went down, got removed or lost its IP address"
touch /tmp/cm_if_unusable_${1}
