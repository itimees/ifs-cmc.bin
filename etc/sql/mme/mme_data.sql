BEGIN TRANSACTION;


-- *******************************************************************************
-- *******************************************************************************
--                    CUSTOMER CONFIGURABLE DEFAULT DATA
-- *******************************************************************************
-- *******************************************************************************


-- ===============================================================================
--      Multizone Playback Configuration
-- ===============================================================================
-- saras head unit
INSERT INTO outputdevices(type, permanent, name, devicepath) VALUES(1, 1, 'rearoutputaudio', 'snd:/dev/snd/pcmC0D1p');
INSERT INTO outputdevices(type, permanent, name, devicepath) VALUES(2, 1, 'rearoutputvideo', 'gf:0000,0000,0?layer=0');

INSERT INTO zones(zoneid, name) SELECT 1, 'local';
INSERT INTO zones(zoneid, name) SELECT 2, 'stream';
INSERT INTO zones(zoneid, name) SELECT 3, 'filestream1';
INSERT INTO zones(zoneid, name) SELECT 4, 'filestream2';
INSERT INTO zones(zoneid, name) SELECT 5, 'filestream3';
INSERT INTO renderers(path) VALUES('/dev/io-media');
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 1, outputdeviceid FROM outputdevices WHERE name='rearoutputaudio';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 1, outputdeviceid FROM outputdevices WHERE name='rearoutputvideo';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 2, outputdeviceid FROM outputdevices WHERE name='rearoutputaudio';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 2, outputdeviceid FROM outputdevices WHERE name='rearoutputvideo';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 3, outputdeviceid FROM outputdevices WHERE name='rearoutputaudio';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 3, outputdeviceid FROM outputdevices WHERE name='rearoutputvideo';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 4, outputdeviceid FROM outputdevices WHERE name='rearoutputaudio';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 4, outputdeviceid FROM outputdevices WHERE name='rearoutputvideo';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 5, outputdeviceid FROM outputdevices WHERE name='rearoutputaudio';
INSERT INTO zoneoutputs(zoneid, outputdeviceid) SELECT 5, outputdeviceid FROM outputdevices WHERE name='rearoutputvideo';

INSERT INTO controlcontexts(zoneid, rendid, name) VALUES( 1, 1, 'default' );
INSERT INTO controlcontexts(zoneid, rendid, name) VALUES( 2, 1, 'stream' );
INSERT INTO controlcontexts(zoneid, rendid, name) VALUES( 3, 1, 'filestream1' );
INSERT INTO controlcontexts(zoneid, rendid, name) VALUES( 4, 1, 'filestream2' );
INSERT INTO controlcontexts(zoneid, rendid, name) VALUES( 5, 1, 'filestream3' );

-- ===============================================================================
--      Device Monitoring
--
-- NOTE: The domain name is not needed on the paths of all devices have the same
-- domian, as set by 'setconf _CS_DOMAIN <domain>'.
-- ===============================================================================
-- saras head unit
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/sd0', 1, 'SDC', 8, 20000);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/usb0', 1, 'USB', 1, 20000);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/usb1', 1, 'USB', 1, 20000);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/ipod0', 1, 'iPod', 4, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/ipod1', 1, 'iPod', 4, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/mtp0', 1, 'PlaysForSure', 4, 20000);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/mtp1', 1, 'PlaysForSure', 4, 20000);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/fs/avrcp0', 1, 'Bluetooth', 4, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/dev/socket', 2, 'INTERNET', 10, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/dev/streamingFiles/files1', 3, 'File1', 4, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/dev/streamingFiles/files2', 4, 'File2', 4, 0);
INSERT INTO slots(path, zoneid, name, slottype, max_lib_entries) VALUES('/dev/streamingFiles/files3', 5, 'File3', 4, 0);


-- ===============================================================================
--      LANGUAGE CONFIGURATION
-- ===============================================================================
-- English
INSERT INTO languages(
      language,
      lang_code,
      unknown,
      unknown_artist,
      unknown_album,
      unknown_genre,
      unknown_category,      
      synchronizing,
      unknown_language,      
      unknown_track,
      unknown_chapter,
      unknown_title,
      unknown_group
      )
values(
      "English",
      "en",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",      
      "Synchronizing",
      "Unknown Language",      
      "Track",
      "Chapter",
      "Title",
      "Group"
      );


-- French
INSERT INTO languages(
      language,
      lang_code,
      unknown,
      unknown_artist,
      unknown_album,
      unknown_genre,
      unknown_category,      
      synchronizing,
      unknown_language,
      unknown_track,
      unknown_chapter,
      unknown_title,
      unknown_group
      )
values(
      "Français",
      "fr",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",     
      "Synchronisation",
      "Langue inconnue",      
      "Piste",
      "Chapitre",
      "Titre",
      "Groupe"
      );

-- German
INSERT INTO languages(
      language,
      lang_code,
      unknown,
      unknown_artist,
      unknown_album,
      unknown_genre,
      unknown_category,      
      synchronizing,
      unknown_language,      
      unknown_track,
      unknown_chapter,
      unknown_title,
      unknown_group
      )
values(
      "Deutsch",
      "de",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",
      "$unknown$",      
      "Synchronisieren",      
      "Unbekannte Sprache",
      "Track",
      "Kapitel",
      "Titel",
      "Gruppe"
      );

-- Select the default language
UPDATE languages SET active=1 WHERE language_id=1;



-- ===============================================================================
--      Media copier encode format configuration
-- ===============================================================================
-- Most platforms should support these:
INSERT INTO encodeformats(name, mime) VALUES('copy', 'copy');
INSERT INTO encodeformats(name, mime, bitrate, extension) VALUES('wave', 'wav', 0, '.wav');

-- Not all platforms support these:
--INSERT INTO encodeformats(name, mime, bitrate, extension) VALUES('Renesas AAC (SH4 only)', 'aac', 0, '.aac');
--INSERT INTO encodeformats(name, mime, bitrate, extension) VALUES('Margi DSP WMA 128000', 'wma', 128000, '.wma');
--INSERT INTO encodeformats(name, mime, bitrate, extension) VALUES('Margi DSP WMA 192000', 'wma', 192000, '.wma');
--INSERT INTO encodeformats(name, mime, bitrate, extension) VALUES('ogg', 'ogg', 0, '.ogg');



-- *******************************************************************************
-- *******************************************************************************
--                          REQUIRED DEFAULT DATA
--                    ** Do not customize this section **
-- *******************************************************************************
-- *******************************************************************************
-- Unknown is always 1
INSERT INTO library_genres(genre_id, genre)
   VALUES(1, (SELECT unknown_genre FROM languages WHERE active=1));
INSERT INTO library_artists(artist_id, artist)
   VALUES(1, (SELECT unknown_artist FROM languages WHERE active=1));
INSERT INTO library_albums(album_id, album)
   VALUES(1, (SELECT unknown_album FROM languages WHERE active=1));
INSERT INTO library_categories(category_id, category)
   VALUES(1, (SELECT unknown_category FROM languages WHERE active=1));
INSERT INTO library_languages(language_id, language)
   VALUES(1, (SELECT unknown_language FROM languages WHERE active=1));
--INSERT INTO library_moods(mood_id, mood)
--   VALUES(1, (SELECT unknown FROM languages WHERE active=1));
INSERT INTO library_composers(composer_id, composer)
   VALUES(1, (SELECT unknown FROM languages WHERE active=1));


COMMIT;
