-- vim: ts=3
BEGIN TRANSACTION;

--
-- Set SQLite's journal mode.
--
-- 'truncate' is the fastest and safest for most configurations.
-- See <http://sqlite.org/pragma.html> for more on this mode.
PRAGMA journal_mode=truncate;


-- The QDB information for this database
CREATE TABLE keyvalueTbl (
   key TEXT PRIMARY KEY,
   value TEXT NOT NULL
);

COMMIT;

