#!/bin/sh
var=$(pidin a | grep restartGPS.lua | grep -v grep )
if [ ! -n "$var" ]; then
   if [ -e /tmp/resetSierraInprogress ]; then
      echo "Resetting Sierra Module is in progress already. do nothing"
      echo "$(date), GMT $(date -tuv): Resetting Sierra Module is in progress already. do nothing" >> /fs/etfs/usr/var/ecall/GPSReset.log
   else
      slay -f vdev-flexgps
      if [ ! -e /tmp/embeddedCallActive ]; then
  	     slay -s KILL AntennaMonitor
	     lua /fs/mmc0/app/bin/restartGPS.lua 3
	  else
		 echo "$(date), GMT $(date -tuv): Sierra AR5550 call active. Cannot reset" >> /fs/etfs/usr/var/ecall/GPSReset.log
         echo "$(date), GMT $(date -tuv): Restarting flexGPS due to GPS issue" >> /fs/etfs/usr/var/ecall/GPSReset.log
	     echo " *** RESTARTING VDEV-FLEXGPS ***"
	     sleep 5
	     nice -n -1 vdev-flexgps -d/hbsystem/multicore/navi/g -b115200 -c/dev/swiusb3 -n -x/bin/restartGPS.sh &
	  fi
   fi
else
  echo "another instance of restartGPS.lua is already running. do nothing"
fi
