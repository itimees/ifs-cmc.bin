#!/usr/bin/lua

-- ===================== initialize head unit ============================
-- this code is to initialize a head unit for normal operation.  it is 
-- supposed to create directories, links, etc..., but is structured so 
-- that if they already exist there is no impact.  it is intended to be
-- run as the last script in boot.sh.  note that this implies that on 
-- a brand new head unit you would have to boot, wait long enough for 
-- the script to run (~60 seconds) and then reboot.
--
-- this should negate the need for 'mmcLinksScript.sh'.  this code supports
-- manufacturing requirements.  the following files are the intended inputs,
--
--           "/etc/init/system_etfs_postinstall.txt"
--           "/etc/init/nav_mmc_postinstall.txt"
--           "/etc/init/system_mmc_postinstall.txt"
--
-- these files are used in the USB update system also, so there is nothing
-- to keep in sync.
--

require "service"
require "lfs"

-- os.execute=print             -- uncomment to print commands vs. executing

io.output(stdout):write("initialize_hu.lua: running...\n")

---------------------------------------------------------------------------
-- Return a table of files to act on (create, link, etc...) from the input
-- file (usually one of the postinstall text files).  sections are delimited
-- with square braces [search_section]
--
-- inputs and returns:
--    input_file: name of the file to scan
--    search_sections: name of section to list files from (create|link|etc...)
--    return: table of strings that contain 'path/filename'
function get_file_list(input_file, search_section)

   local rvalue = {}
   local current_section = nil
   local index = 1

	-- check if file exists first
	if nil == io.open(input_file,"r") then
	   io.output(stdout):write("initialize_hu.lua: cannot open "..input_file.."\n")
	   return nil
	end

   -- gather the list into the table
   local desired_section = "["..search_section.."]"
   local ss = "^%["..search_section.."]"
   for line in io.lines(input_file) do
      if string.match(line, ss) then
         current_section = desired_section
      elseif string.match(line, "%[%a+]") then
         current_section = nil
      else
         if current_section == desired_section then
            if string.match(line, "^/[%a%d/]") then
               rvalue[index]=line
               index = index + 1
            end
         end
      end
   end -- for

   return rvalue or nil

end

---------------------------------------------------------------------------
-- Check if links and directories exist based on the input
-- file (usually one of the postinstall text files).  sections are delimited
-- with square braces [search_section]
--
-- inputs and returns:
--    section: section to work on
--    input_file: name of the file to scan
--    return: none
function check(section, input_file)

   local l=get_file_list(input_file, section)
 
   if section == "create" then
      for index,value in ipairs(l) do
         local u = string.gsub(value,"[/ ]+$", "")
         local y = lfs.attributes(u)
         if y == nil then
            local g = "mkdir -p "..value
            local e = os.execute(g)
            --e = lfs.mkdir(u)
            if e == 0 then
               io.output(stdout):write("initialize_hu.lua:  repaired directory '"..u.."'\n")
            else
               io.output(stdout):write("initialize_hu.lua:  unable to repair directory '"..u.."'\n")
            end
         end
      end -- for
   elseif section == "link" then
      for index,value in ipairs(l) do
         local p = string.gsub(value,"[ ]+", "") -- strip all spaces
         local u = string.gsub(p,"[%d%a_/.]+,", "") -- get all past the comma seperator
         local y = lfs.symlinkattributes(u)
         if y == nil then
            local g = "ln -sf "..string.gsub(value,", ", " ")
            local e=os.execute(g)
            if e == 0 then
               io.output(stdout):write("initialize_hu.lua:  repaired link '"..u.."'\n")
            else
               io.output(stdout):write("initialize_hu.lua:  unable to repair link '"..u.."'\n")
            end
         end
      end -- for
   end

end

-- perform operations on ETFS

-- create all ETFS files
local etfs_file="/etc/init/system_etfs_postinstall.txt"
-- io.output(stdout):write("initialize_hu.lua: using '"..etfs_file.."' to check ETFS directories and links\n")
check("create", etfs_file)
check("link", etfs_file)

q = os.execute("cp /fs/mmc0/app/share/trace/* /HBpersistence/")
if(q~=0) then
   io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/trace/*.hbtc to /HBpersistence/ ("..q..")\n")
end

os.execute("chmod 777 /HBpersistence/*.hbtc")
if(q~=0) then
   io.output(stdout):write("initialize_hu.lua: Error while changing permissions on /HBpersistence/*.hbtc ("..q..")\n")
end

if(os.getenv("VARIANT_SDARS") == "YES") then 
   io.output(stdout):write("initialize_hu.lua:  VARIANT_SDARS \n")
   
   local q = os.execute("cp -RX /fs/mmc0/app/share/sdars/traffic /fs/etfs/usr/var/sdars/")
   if(q~=0) then
      io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/traffic to /fs/etfs/usr/var/sdars/ (no overwrite) ("..q..")\n")
   end

   q = os.execute("cp -RX /fs/mmc0/app/share/sdars/sports /fs/etfs/usr/var/sdars/")
   if(q~=0) then
      io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/sports to /fs/etfs/usr/var/sdars/ (no overwrite) ("..q..")\n")
   end

   q = os.execute("cp -RX /fs/mmc0/app/share/sdars/sportsservice /fs/etfs/usr/var/sdars/")
   if(q~=0) then
      io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/sportsservice to /fs/etfs/usr/var/sdars/ (no overwrite) ("..q..")\n")
   end

   q = os.execute("cp -RX /fs/mmc0/app/share/sdars/channelart /fs/etfs/usr/var/sdars/")
   if(q~=0) then
      io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/channelart to /fs/etfs/usr/var/sdars/ (no overwrite) ("..q..")\n")
   end
   
   q = os.execute("cp -RX /fs/mmc0/app/share/sdars/phonetics /fs/etfs/usr/var/sdars/")
   if(q~=0) then
      io.output(stdout):write("initialize_hu.lua: Error while copying /fs/mmc0/app/share/sdars/phonetics to /fs/etfs/usr/var/sdars/ (no overwrite) ("..q..")\n")
   end   

   os.execute("chmod  -R 666 /fs/etfs/usr/var/sdars")
end

-- perform operations on MMC0
r,e = service.invoke("com.harman.service.OmapTempService", "getOmapTemperature")
if (e == nil) and (r.omapTemp ~= nil) then
   if (tonumber(r.omapTemp) > -20) then
      if 0 ~= os.execute("mount -uw /fs/mmc0") then
         io.output(stdout):write("initialize_hu.lua: can't remount mmc0 as writeable file system\n")
         os.exit(-1)
      end
--[[
      if 0 ~= os.execute("mount -uw /fs/mmc1") then
         io.output(stdout):write("initialize_hu.lua: can't remount mmc1 as writeable file system\n")
         os.exit(-1)
      end
--]]
   else
      io.output(stdout):write("initialize_hu.lua: brrrr... too cold - exiting\n")
      os.exit(-1)
   end
else
   io.output(stdout):write("initialize_hu.lua: ",e)
end

-- create all nav mmc directories and links
local mmc_file="/etc/init/nav_mmc_postinstall.txt"
-- io.output(stdout):write("initialize_hu.lua: using '"..mmc_file.."' to check MMC0 directories and links\n")
check("create", mmc_file)
check("link", mmc_file)

-- create all system mmc directories and links
local mmc_file="/etc/init/system_mmc_postinstall.txt"
-- io.output(stdout):write("initialize_hu.lua: using '"..mmc_file.."' to check MMC0 directories and links\n")
check("create", mmc_file)
check("link", mmc_file)

--[[
-- perform operations on MMC1
--]]

-- make MMC file systems read only again
if 0 ~= os.execute("mount -ur /fs/mmc0") then
   io.output(stdout):write("initialize_hu.lua: Error while trying to make mmc0 read only again.  Please reset unit to correct\n")
end

--[[
if 0 ~= os.execute("mount -ur /fs/mmc1") then
   io.output(stdout):write("initialize_hu.lua: Error while trying to make mmc1 read only again.  Please reset unit to correct\n")
end
--]]
