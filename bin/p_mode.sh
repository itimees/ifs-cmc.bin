#!/bin/sh

#
# Determine the boot mode from the third byte
# of the "swdl" section of the FRAM.  "P" /
# indicates that we are in P mode.  Anything 
# else is not processed in this file.
#
inject -e -i /dev/mmap/swdl -f /tmp/p_mode -o 2 -s 1
P_MODE=`cat /tmp/p_mode`
echo "p_mode: Mode is $P_MODE\n"
rm -f /tmp/p_mode

if [ "$P_MODE" != "P" ]; then
  exit 0
fi
echo "p_mode:Software P Mode Detected\n"

#
# This function displays following screens
#1. Screen for P mode
#2. if USB stick with iso image is present then it displays a screen which request user either to remove the stick within a short time OR it will update the ISO  
#
# $1 = screen 1 or screen 2         $2 = Where was quit called from
quit(){

   echo $2   
# if option 1 then display screen option P 
# Note, Only after giving IG cycle you can come out of this  
   if [ $1 == 1 ]; then
      echo "p_mode:quit with option 1"
      splash  -t 600 /etc/pmode.png &
      while true; do
          sleep 1
      done
   elif [ $1 == 2 ]; then
 # if option 2 then display the screen which request user to remove stick with time OR 
 # USB stick has valid image do you want to load? or remove the stick within time 
      echo "p_mode:quit with option 2"
      splash -t 600 /etc/pmode_update_image.png &
   fi
   
}

#
# Get the USB stick mounted.  Large USB devices can take
# some number of seconds to enumerate and auto mount.
#
check_usb_device()
{

    echo $1
    #
    # Define the most basic part of the USB stick layout and where the
    # ISO will be mounted.  Use my PID and a random number in the
    # mount path to insure no collisions with anything already
    # mounted on the target.
    #
    USB_STICK=/fs/usb0
    ISO_NAME=swdl.upd
    ISO_PATH=/mnt/swdl.upd.$$.$RANDOM
    KEYS_DIR=/etc/keys
    waitfor $USB_STICK/$ISO_NAME 30
    echo "p_mode:enum usb devices completed\n"
    #
    # Locate the ISO either in the root of the usb stick or in the
    # old location of swdl/swdl.upd.  We will exit if we cannot find
    # a suitable ISO image.
    #
    if [ -e $USB_STICK/$ISO_NAME ]; then
      ISO=$USB_STICK/$ISO_NAME
    else
      if [ -e $USB_STICK/swdl/$ISO_NAME ]; then
        ISO=$USB_STICK/swdl/$ISO_NAME
      else
        quit 1 "p_mode:USB with ISO image is not available"
      fi
    fi
    echo "p_mode:check usb device exited"
}
#pet the watchdog 
#this is to ensure that the mode remains A even if device is reseted before the usb detection
echo "p_mode:mode changed to A"
echo A>/tmp/p_mode
inject -i /tmp/p_mode -f /dev/mmap/swdl -o 2
rm -f /tmp/p_mode 
#echo "traceChannel(1)" > /dev/ipc/debug
#echo "verbose(7)" > /dev/ipc/debug
#echo "enableTrace(2)" > /dev/ipc/debug
lua -s -b /bin/mongrel_forever.lua&
#T="$!"
#echo "$T"
#mount  -uw /fs/mmc0/
#pidin -p "$T" fds >  /fs/mmc0/pidin.txt
#pidin -p "$T" fds
echo "p_mode:enum usb devices started\n"
enum-devices -c /etc/system/enum/common
#  Start the usb overcurrent monitor utility
/usr/bin/usb_hub_oc -p 10 -i 10 -d 500 &

splash  -t 40 /etc/pmode_checking_usb.png &
check_usb_device " p_mode:Trying to Detect USB 1st time\n"
# usb with valid image is detected - sleep while waiting USB and then again check if USB is still present with valid image
echo "p_mode:USB device is present: call quit with option 2"

quit 2 "p_mode:This USB stick has ISO image. do you want to load?"

sleep 30
echo " p_mode:calling USB check device 2nd time"
check_usb_device "p_mode:Trying to Detect USB 2nd time\n"

#
#USB with valid image is still available. let us change the mode to "U" (update ) and perform hard reset
#
echo U>/tmp/p_mode
inject -i /tmp/p_mode -f /dev/mmap/swdl -o 2
rm -f /tmp/p_mode
echo "p_mode:Mode change U\n"
echo "swdl contents"
hd -v -n8 /fs/fram/swdl
#issue reset so that update can be started
echo -n "\021" >/dev/ipc/ch4
echo "p_mode:exit script\n"
exit 0
