#!/bin/sh

echo "$0 running" > /dev/ser3

# Slay QDB
slay -f -sterm qdb

for x in $*
do
   echo "Corruption detected in qdb database $x" > /dev/ser3

   # Check if /usr/var/logs exists
   if [[ ! -d /usr/var/logs ]]; then
      mkdir -p /usr/var/logs
   fi

   # Persist what was corrupted and when it happened so we know why we reset.
   echo "[$(date)] qdb $x database corrupted" >> /usr/var/logs/qdb_reset.log

   if [[ $x = "keyvalue" ]]; then
      echo "Deleting /usr/var/qdb/key_value*" > /dev/ser3
      rm -f /usr/var/qdb/key_value*
   elif [[ $x = "mme" ]]; then
      echo "Deleting /usr/var/qdb/mme*" > /dev/ser3
      rm -f /usr/var/qdb/mme*
   elif [[ $x = "pim" ]]; then
      echo "Deleting /usr/var/qdb/pim*" > /dev/ser3
      rm -f /usr/var/qdb/pim*
   else
      echo "Unknown database" > /dev/ser3
      echo "Attempting to delete /usr/var/qdb/${x}*" > /dev/ser3
      rm -f /usr/var/qdb/${x}*
   fi
done

# Throw popup letting user know we will reset.
. /tmp/envars.sh
dbus-send --type=method_call --dest='com.harman.service.HMIGateWay' /com/harman/service/HMIGateWay com.harman.ServiceIpc.Invoke string:"displayGlobalPopup" string:'{"timeout":20000, "showonDisplayoff":true, "showRunningCategory":true, "msg":"Refreshing Database\nRadio will restart shortly..."}'

# TODO:  Create an immediate reset request in OnOff to safely shutdown xmApp.
echo "Resetting" > /dev/ser3
reset

