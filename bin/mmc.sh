#!/bin/sh

# Exit codes:
#   0 = OK
#   1 = MMC driver already running
#   2 = Failed to start MMC driver
#   3 = Error partitioning MMC
#   4 = Error re-reading partition table MMC
#   5 = Error formatting QNX6 filesystem(s) onto MMC
#   6 = MMC failed to mount filesystem(s)
#   7 = Filesystem check failed 
#  10 = Invalid option
#
# Command line options:
#   start - Start driver (defaults to update mode)
#   erase - Force erase/format of MMC at startup
#   format - Same as erase
#   stop - Stop the driver
#   umount - Unmount the filesystems (depricated for CMC)
#   mount - Mount the filesystems (depricated for CMC)
#   application - When scrips is used in non update mode 
#   checkfs - check the file system integrity

NAME_FILE=/tmp/mmc.name
PID_FILE=/tmp/mmc.pid
RAW_DEV="/dev/mmc0"
READ_DISTURB_DEV="/dev/mmc0t77"
APPLICATION_DEV="/dev/mmc0t177"
XLET_DEV="/dev/mmc0t178"
XLET_DATA_DEV="/dev/mmc0t177.1"
OTA_DATA_DEV="/dev/mmc0t178.1"
APPLICATION_PATH="/fs/mmc0"
XLET_PATH="/fs/mmc1"
XLET_DATA_PATH="/fs/mmc2"
OTA_DATA_PATH="/fs/mmc3"
MMC1_BK_PATH="/fs/mmc0/mmc1_bk"
FOTA_OLD_LOCATION="/fs/mmc0/update"
FOTA_NEW_LOCATION="/fs/mmc3/update"
FOTA_NEW_LOCATION_BK="/fs/mmc0/mmc3_update_bk"
FOTA_DP="/fs/mmc3/update/ota/rb/dp"

#
# This function does general clean-up and reporting in case of errors
#
quit() {
   echo "MMC.SH: Quitting $1"

   sf=$1

   # unmount the application path
   if [[ -e $APPLICATION_PATH ]]; then
      umount -fv $APPLICATION_PATH
   fi
   
   # unmount the xlet app path
   if [[ -e $XLET_PATH ]]; then
      umount -fv $XLET_PATH
   fi

   # unmount the xlet data path
   if [[ -e $XLET_DATA_PATH ]]; then
      umount -fv $XLET_DATA_PATH
   fi
   
   # unmount the ota data path
   if [[ -e $OTA_DATA_PATH ]]; then
      umount -fv $OTA_DATA_PATH
   fi
   
   # unload the driver from memory
   if [[ -e $RAW_DEV ]]; then
   
      # slay the driver
      slay `cat $PID_FILE`
      
      # wait for it to end fully
      for k in 1 2 3 4 5 6 ; do
         
         waitfor $RAW_DEV 1  > /dev/null 2>&1
         if [[ $? -eq 0 ]]; then
            echo "MMC.SH: Waiting for $RAW_DEV to disappear on try $k"
         else
            break
         fi
         
         sleep 5

      done 

      # handle result
      if [[ $k -lt 5 ]]; then
         echo "MMC.SH: Driver for $RAW_DEV successfully slayed"
         
         # remove the pid and name files
         rm -f $PID_FILE
         rm -f $NAME_FILE

      else
         echo "MMC.SH: Unable to slay $RAW_DEV driver"
         sf=127
      fi

   else
      echo "MMC.SH: Driver for $RAW_DEV was not running"
   fi

   exit $sf
}

#
#  Back Up MMC1 partition data in MMC0 during Take Back repartitioning
#
mmc1TBbackup() {
   if [[ -e $APPLICATION_DEV && -e $XLET_DEV ]]; then
      echo "MMC.SH: Back Up MMC1 under /fs/mmc0/mmc1_bk/ before doing Take Back repartitioning"

      mount -t qnx6 $APPLICATION_DEV $APPLICATION_PATH
      mount -t qnx6 $XLET_DEV $XLET_PATH
		
      #wait for file system to get mounted
      waitfor $APPLICATION_PATH > /dev/null 2>&1
      waitfor $XLET_PATH > /dev/null 2>&1

      cp -Rfc $XLET_PATH/* $MMC1_BK_PATH/
      sync
      
      echo "MMC.SH: Back Up MMC1 was Done!!!"

      if [[ -e $OTA_DATA_DEV ]]; then
         mount -t qnx6 $OTA_DATA_DEV $OTA_DATA_PATH
         #wait for file system to get mounted
         waitfor $OTA_DATA_PATH > /dev/null 2>&1
         
         if [[ -e $FOTA_NEW_LOCATION && ! -e $FOTA_OLD_LOCATION ]]; then
            echo "MMC.SH: Back Up $FOTA_NEW_LOCATION under $FOTA_NEW_LOCATION_BK before repartitioning"
            
            cp -Rfc $FOTA_NEW_LOCATION/* $FOTA_NEW_LOCATION_BK/
            sync
            
            echo "MMC.SH: Back Up $FOTA_NEW_LOCATION under $FOTA_NEW_LOCATION_BK was Done!!!"
         fi
         
         umount -f $OTA_DATA_PATH
      fi

      umount -f $APPLICATION_PATH
      umount -f $XLET_PATH
      
   else
      echo "MMC.SH: TB MMC1/0 not available to take back up"
   fi
}

#
#  Restore MMC1 partition data from MMC0 after Take Back repartitioning
#
mmc1TBrestore() {

   # In FOTA_NEW_LOCATION_BK or FOTA_OLD_LOCATION, only one can present both can't present, because of the way FOTA_NEW_LOCATION_BK was done
   if [[ -e $FOTA_NEW_LOCATION_BK ]]; then
      echo "MMC.SH: Move $FOTA_NEW_LOCATION_BK to $FOTA_NEW_LOCATION"
      
      mkdir -p $FOTA_NEW_LOCATION
      cp -Rf $FOTA_NEW_LOCATION_BK/* $FOTA_NEW_LOCATION/
      rm -rf $FOTA_NEW_LOCATION_BK
      sync
      
      echo "MMC.SH: Moving $FOTA_NEW_LOCATION_BK to $FOTA_NEW_LOCATION is Done!!!"
   fi

  #Custom operation for FOTA to move from MMC0 to MMC3 during restoring
   if [[ -e $FOTA_OLD_LOCATION ]]; then
      echo "MMC.SH: Move $FOTA_OLD_LOCATION to $FOTA_NEW_LOCATION"

      mkdir -p $FOTA_NEW_LOCATION
      cp -Rf $FOTA_OLD_LOCATION/* $FOTA_NEW_LOCATION/
      cp /fs/mmc0/dupd/rb/ota/rb_ua_mmc3.conf /fs/mmc0/dupd/rb/ota/rb_ua.conf
      
      #Modify /fs/mmc3/update/ota/rb/dp to point to mmc3 instead of mmc0
      if [[ -e $FOTA_DP ]]; then
         #for safety remove file under tmp
         rm /tmp/dp_modified
         awk '{ gsub("mmc0","mmc3"); print }' $FOTA_DP >> /tmp/dp_modified
         cp /tmp/dp_modified $FOTA_DP
         echo "$FOTA_DP modified to point mmc3 instead of mmc0"
         cat $FOTA_DP
      fi

      rm -rf $FOTA_OLD_LOCATION
      sync

      echo "MMC.SH: Moving $FOTA_OLD_LOCATION to $FOTA_NEW_LOCATION is Done!!!"
   fi  

   if [[ -e $MMC1_BK_PATH ]]; then
      echo "MMC.SH: Restore MMC1 data from /fs/mmc0/mmc1_bk/ after Take Back repartitioning"       
 
      cp -Rf $MMC1_BK_PATH/* $XLET_PATH/
      rm -rf $MMC1_BK_PATH/
      sync
      
      echo "MMC.SH: Restore MMC1 was Done!!!"
   fi
}

#
#  partitions the MMC device
#
partition() {
   echo "MMC.SH: Creating partitions on $RAW_DEV"

   # Grab the size of the MMC device
   MMC_SIZE=`fdisk $RAW_DEV query -T`
   ((MMC_END_CELL=MMC_SIZE-1))

   # make sure we got a response from fdisk
   if [[ MMC_END_CELL -lt 5 ]]; then
      quit 3
   fi
   
   # Get partition information.  If you change the numbers below, you 
   # must also change the MMC update script in the software download 
   # located here: /<project_root>/tcfg/omap/scripts/formatMMC.sh
   READ_DISTURB_SECTORS=0,1
   if [[ $MMC_SIZE -gt 15000 ]] then
      APPLICATION_SECTORS=2,10294
      # MICRON=15007, SANDISK=15187
      XLET_SECTORS=10295,14000
      EXTENDED_SECTORS=14001,$MMC_END_CELL
      XLET_DATA_SECTORS=200
      ((OTA_DATA_SECTORS=MMC_END_CELL-14201+1))
   else
      APPLICATION_SECTORS=2,6597
      # MICRON=7503, SANDISK=7575
      XLET_SECTORS=6598,7050
      EXTENDED_SECTORS=7051,$MMC_END_CELL
      XLET_DATA_SECTORS=10
      ((OTA_DATA_SECTORS=MMC_END_CELL-7061+1))
   fi

   # add partitions to table   
   echo "MMC.SH: Partitions on $MMC_SIZE cell device are:"
   echo "MMC.SH:    ARDS       = $READ_DISTURB_SECTORS"
   echo "MMC.SH:    SYSTEM     = $APPLICATION_SECTORS"
   echo "MMC.SH:    XLETS      = $XLET_SECTORS"
   echo "MMC.SH:    XLET DATA  = $XLET_DATA_SECTORS"
   echo "MMC.SH:    OTA DATA   = $OTA_DATA_SECTORS"
   
   fdisk $RAW_DEV delete -a

   fdisk $RAW_DEV add -s 1 -t 77 -c $READ_DISTURB_SECTORS
   if [[ $? != 0 ]]; then 
      quit 3
   fi
   
   fdisk $RAW_DEV add -s 2 -t 177 -c $APPLICATION_SECTORS
   if [[ $? != 0 ]]; then 
     quit 3
   fi
   
   fdisk $RAW_DEV add -s 3 -t 178 -c $XLET_SECTORS
   if [[ $? != 0 ]]; then 
     quit 3
   fi
   
   fdisk $RAW_DEV add -s 4 -t 133 -c $EXTENDED_SECTORS
   if [[ $? != 0 ]]; then 
     quit 3
   fi
   
   fdisk $RAW_DEV add -s 4 -e 1 -t 177 -n $XLET_DATA_SECTORS
   if [[ $? != 0 ]]; then 
     quit 3
   fi
   
   fdisk $RAW_DEV add -s 4 -e 2 -t 178 -n $OTA_DATA_SECTORS
   if [[ $? != 0 ]]; then 
     quit 3
   fi
   
   # Rescan the partition table
   mount -e $RAW_DEV

   # Make sure they are ready
   if [[ -e $READ_DISTURB_DEV && -e $APPLICATION_DEV && -e $XLET_DEV && -e $XLET_DATA_DEV && -e $OTA_DATA_DEV ]]; then
      echo "MMC.SH: New partition table completed"
   else
      echo "MMC.SH: New partition table not detected"
      quit 4
   fi
   
}

format() {
   echo "MMC.SH: Formatting partitions on $RAW_DEV"

   if [[ $MMC_TB -ne 1 ]]; then
      if [[ -e $APPLICATION_DEV ]]; then
         # Format the partitions
         echo "MMC.SH: Formatting $APPLICATION_DEV"
         mkqnx6fs -q $APPLICATION_DEV
         if [[ $? == 0 ]]; then
            echo "MMC.SH: Successfully formatted $APPLICATION_DEV"
         else
            quit 5
         fi
      else
         echo "MMC.SH: No partition detected for format operation on $APPLICATION_DEV"
      fi
   else
      echo "MMC.SH: SKIP $APPLICATION_DEV partition format as Take Back Software was detected"
   fi

   if [[ -e $XLET_DEV ]]; then
      # Format the partitions
      echo "MMC.SH: Formatting $XLET_DEV"
      mkqnx6fs -q $XLET_DEV
      if [[ $? == 0 ]]; then
         echo "MMC.SH: Successfully formatted $XLET_DEV"
      else
         quit 5
      fi
   else
      echo "MMC.SH: No partition detected for format operation on $XLET_DEV"
   fi
   
   if [[ -e $XLET_DATA_DEV ]]; then
      # Format the partitions
      echo "MMC.SH: Formatting $XLET_DATA_DEV"
      mkqnx6fs -q $XLET_DATA_DEV
      if [[ $? == 0 ]]; then
         echo "MMC.SH: Successfully formatted $XLET_DATA_DEV"
      else
         quit 5
      fi
   else
      echo "MMC.SH: No partition detected for format operation on $XLET_DATA_DEV"
   fi
   
   if [[ -e $OTA_DATA_DEV ]]; then
      # Format the partitions
      echo "MMC.SH: Formatting $OTA_DATA_DEV"
      mkqnx6fs -q $OTA_DATA_DEV
      if [[ $? == 0 ]]; then
         echo "MMC.SH: Successfully formatted $OTA_DATA_DEV"
      else
         quit 5
      fi
   else
      echo "MMC.SH: No partition detected for format operation on $OTA_DATA_DEV"
   fi
}

#
# Parse through the command line options (start, stop, erase).  No command line
# options is equivalent to "start".  This parsing is not checked well, so don't
# do anything dumb.
#
while [[ "$1" != "" ]]; do
   case $1 in
      stop)
         quit 0
      ;;
      format)
         MMC_INIT=1
      ;;
      erase)
         MMC_INIT=1
      ;;
      start)
      ;;
      umount)
         umount -f $APPLICATION_PATH
         umount -f $XLET_PATH
         umount -f $XLET_DATA_PATH
         umount -f $OTA_DATA_PATH
         exit 0
      ;;
      mount)
         mount -t qnx6 $APPLICATION_DEV $APPLICATION_PATH 
         waitfor $APPLICATION_PATH 1 > /dev/null 2>&1
         if [[ ! -e $APPLICATION_PATH ]]; then
            quit 6
         fi
         mount -t qnx6 $XLET_DEV $XLET_PATH 
         waitfor $XLET_PATH 1 > /dev/null 2>&1
         if [[ ! -e $XLET_PATH ]]; then
            quit 6
         fi
         mount -t qnx6 $XLET_DATA_DEV $XLET_DATA_PATH 
         waitfor $XLET_DATA_PATH 1 > /dev/null 2>&1
         if [[ ! -e $XLET_DATA_PATH ]]; then
            quit 6
         fi
         mount -t qnx6 $OTA_DATA_DEV $OTA_DATA_PATH 
         waitfor $OTA_DATA_PATH 1 > /dev/null 2>&1
         if [[ ! -e $OTA_DATA_PATH ]]; then
            quit 6
         fi


         exit 0  
      ;;   
      fota2Special)
         FOTA2_SPECIAL=1
      ;;      
      application)
         APPLICATION_MODE=1     
      ;;
      checkfs)
         CHECK_FS=1
      ;;
      *)
         exit 10
      ;;
  esac
  shift
done

# Make sure the driver hasn't already been started
if [[ -e $PID_FILE ]]; then
   echo "MMC.SH: Error - driver for $RAW_DEV is already running"
   exit 11
else
   # Define MMC driver options
   if [[ $APPLICATION_MODE -eq 1 ]]; then
      echo "MMC.SH: Using application mode settings"
      MMC_DRIVER_FNAME=devb-mmcsd-omap3730teb
      # possible clock speeds: 24000000 48000000
      MMC_CLOCK=48000000
      AUTOMOUNT=1
      MMCSD_OPTIONS="mmcsd ioport=0x480b4000,ioport=0x48056000,irq=86,dma=30,dma=47,dma=48,noac12,normv,clock=$MMC_CLOCK"
      CAM_OPTIONS="cam cache"
      BLK_OPTIONS="blk noatime,cache=512k,lock,normv,rmvto=none,automount=mmc0t177:/fs/mmc0:qnx6:ro,automount=mmc0t178:/fs/mmc1:qnx6:ro,automount=mmc0t177.1:/fs/mmc2:qnx6,automount=mmc0t178.1:/fs/mmc3:qnx6"
      DISK_OPTIONS="disk name=mmc"
   else
      echo "MMC.SH: Using update mode settings"
      MMC_DRIVER_FNAME=devb-mmcsd-omap3730teb
      # possible clock speeds: 24000000 48000000
      MMC_CLOCK=48000000
      MMCSD_OPTIONS="mmcsd ioport=0x480b4000,ioport=0x48056000,irq=86,dma=30,dma=47,dma=48,noac12,normv,clock=$MMC_CLOCK"
      CAM_OPTIONS="cam cache"
      BLK_OPTIONS="blk noatime,cache=8m"
      DISK_OPTIONS="disk name=mmc"
   fi

   # start up the driver
   echo "MMC.SH: $MMC_DRIVER_FNAME $MMCSD_OPTIONS $CAM_OPTIONS $BLK_OPTIONS $DISK_OPTIONS"
   $MMC_DRIVER_FNAME $MMCSD_OPTIONS $CAM_OPTIONS $BLK_OPTIONS $DISK_OPTIONS &
   MMC_PID=$!

   # Make sure we have a raw device
   waitfor $RAW_DEV 5
   if [[ -e $RAW_DEV ]]; then
      echo "MMC.SH: Driver started"
      echo -n $MMC_DRIVER_FNAME > $NAME_FILE
      echo -n $MMC_PID > $PID_FILE

      # if no partition exists, then force creation and a format
      waitfor $APPLICATION_DEV 5 > /dev/null 2>&1
      waitfor $XLET_DEV 5 > /dev/null 2>&1
      waitfor $XLET_DATA_DEV 5 > /dev/null 2>&1
      waitfor $OTA_DATA_DEV 5 > /dev/null 2>&1
      waitfor $READ_DISTURB_DEV 5 > /dev/null 2>&1
      
      #If XLET_DATA_DEV_SIZE=100M then we need to do Re-partition to increase the MMC3 size by 90M and decrease the MMC2 size to 10MB
      XLET_DATA_DEV_SIZE=`df -h $XLET_DATA_DEV | awk '{print $2}'`
      
      if [[ -e $READ_DISTURB_DEV && -e $APPLICATION_DEV && -e $XLET_DEV && -e $XLET_DATA_DEV && -e $OTA_DATA_DEV && ("100M" !=  $XLET_DATA_DEV_SIZE) ]]; then
         # Place the driver PID and name  into /tmp for slaying later
         echo "MMC.SH: Partitions detected"
      else
         #if MMC_INIT=1 means we would have got format/erase so we should not do the TB handling
         if [[ -e $APPLICATION_DEV && $MMC_INIT -ne 1 ]]; then
            echo "MMC.SH: MMC Take Back (use \"mmc.sh format\" except for mmc0 partition)"
            MMC_TB=1
         else
            echo "MMC.SH: No partitions (use \"mmc.sh format\")"
         fi
         MMC_INIT=1
      fi
   else
      echo "MMC.SH: No raw MMC device, driver failed to start"
      exit 2
   fi
fi

# create partitions and format if needed
if [[ ($MMC_INIT -eq 1) && ($APPLICATION_MODE -ne 1) && ($FOTA2_SPECIAL -ne 1) ]]; then

   if [[ $MMC_TB -eq 1 ]]; then
      mmc1TBbackup
   fi

   echo "MMC.SH: Initializing MMC"
   partition
   format
   CHECK_FS=1
fi

# Check the filesystems if requested (do before mounting)
if [[ $CHECK_FS -eq 1 ]]; then
   echo "MMC.SH: Checking file systems on $RAW_DEV (must re-mount or re-start)"

   umount -f $APPLICATION_PATH
   umount -f $XLET_PATH
   umount -f $XLET_DATA_PATH
   umount -f $OTA_DATA_PATH

   if [[ $MMC_TB -ne 1 ]]; then
      echo "MMC.SH: Checking file system on $APPLICATION_DEV"
      chkqnx6fs -fv $APPLICATION_DEV
      if [[ $? != 0 ]]; then
         # Clean up any stray clusters found in chkqnx6fs.
         rm -f $APPLICATION_PATH/lost+found
         quit 7
      fi
   else
      echo "MMC.SH: SKIP $APPLICATION_DEV file system checking as Take Back Software was detected"
   fi
   
   echo "MMC.SH: Checking file system on $XLET_DEV"
   chkqnx6fs -fv $XLET_DEV
   if [[ $? != 0 ]]; then
      # Clean up any stray clusters found in chkqnx6fs.
      rm -f $XLET_PATH/lost+found
      quit 7
   fi
   
   echo "MMC.SH: Checking file system on $XLET_DATA_DEV"
   chkqnx6fs -fv $XLET_DATA_DEV
   if [[ $? != 0 ]]; then
      # Clean up any stray clusters found in chkqnx6fs.
      rm -f $XLET_DATA_PATH/lost+found
      quit 7
   fi

   echo "MMC.SH: Checking file system on $OTA_DATA_DEV"
   chkqnx6fs -fv $OTA_DATA_DEV
   if [[ $? != 0 ]]; then
      # Clean up any stray clusters found in chkqnx6fs.
      rm -f $OTA_DATA_PATH/lost+found
      quit 7
   fi
   
   AUTOMOUNT=0
fi

# mount the file system(s) if needed
if [[ $AUTOMOUNT -ne 1 ]]; then
   echo "MMC.SH: Mounting file systems on $RAW_DEV"
   mount -t qnx6 $APPLICATION_DEV $APPLICATION_PATH
   mount -t qnx6 $XLET_DEV $XLET_PATH
   mount -t qnx6 $XLET_DATA_DEV $XLET_DATA_PATH
   mount -t qnx6 $OTA_DATA_DEV $OTA_DATA_PATH
fi

# check if successfully mounted
waitfor $APPLICATION_PATH > /dev/null 2>&1
if [[ -e $APPLICATION_PATH ]]; then
   echo "MMC.SH: $APPLICATION_PATH detected"
else
   echo "MMC.SH: $APPLICATION_PATH not detected"
   quit 6
fi

waitfor $XLET_PATH > /dev/null 2>&1
if [[ -e $XLET_PATH ]]; then
   echo "MMC.SH: $XLET_PATH detected"
else
   echo "MMC.SH: $XLET_PATH not detected"
   quit 6
fi

if [[ ($FOTA2_SPECIAL -eq 1) ]]; then
   echo "MMC.SH: FOTA2_SPECIAL, MMC driver launched successfully"
   # Exit from MMC.sh as we mounted /fs/mmc0,mmc1 for Special FOTA2 update,
   # mmc2,mmc3 may not present in all special FOTA2 updates
   exit 0
fi

waitfor $XLET_DATA_PATH > /dev/null 2>&1
if [[ -e $XLET_DATA_PATH ]]; then
   echo "MMC.SH: $XLET_DATA_PATH detected"
else
   echo "MMC.SH: $XLET_DATA_PATH not detected"
   quit 6
fi

waitfor $OTA_DATA_PATH > /dev/null 2>&1
if [[ -e $OTA_DATA_PATH ]]; then
   echo "MMC.SH: $OTA_DATA_PATH detected"
else
   echo "MMC.SH: $OTA_DATA_PATH not detected"
   quit 6
fi

if [[ ($APPLICATION_MODE -ne 1) && ($FOTA2_SPECIAL -ne 1) ]]; then
   mmc1TBrestore
fi

exit 0
