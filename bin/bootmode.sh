#!/bin/sh

# Run the differential update code
dupd(){
   echo "DUPD: PROCESSING DIFFERENTIAL UPDATE"
   waitfor /fs/mmc0/dupd/installer
   if [ $? -eq 0 ]; then
      cd /fs/mmc0/dupd/installer
      waitfor /usr/bin/lua
      if [ $? -eq 0 ]; then
         PATH=/fs/mmc0/dupd/bin:$PATH LD_LIBRARY_PATH=/fs/mmc0/dupd/lib:$LD_LIBRARY_PATH lua master_updater.lua
         while :; do
            echo "DUPD: SHOULD HAVE RESET BEFORE THIS"   -- TODO add error screen
            sleep 15
         done
      else
         echo "DUPD: LUA EXECUTABLE NOT FOUND"
      fi
   else
      echo "DUPD: NO MMC FOUND"
   fi
}
#
# Determine the boot mode from the third byte
# of the "swdl" section of the FRAM.  A "U" 
# indicates that we are in Update mode.  Anything 
# else indicates otherwise.
#
inject -e -i /dev/mmap/swdl -f /tmp/bootmode -o 2 -s 1
BOOTMODE=`cat /tmp/bootmode`
echo "BOOTMODE.SH: Bootmode flag is $BOOTMODE"
rm -f /tmp/bootmode

case $BOOTMODE in
U)
   echo "USB UPDATE BOOT MODE ($BOOTMODE)"
   ;;
A)
   echo "APPLICATION BOOT MODE ($BOOTMODE)"
   exit 0
   ;;
D)
   echo "DIFFERENTIAL UPDATE BOOT MODE ($BOOTMODE)"
   /bin/sh /bin/d_mode.sh
   echo "BOOTMODE.SH: ERROR: No Come Back from d_mode.sh this msg we should not see"
   exit 0
   ;;
R)
   echo "ROLLBACK BOOT MODE ($BOOTMODE)"
   dupd
   ;;
*)
   echo "UNKNOWN BOOT MODE ($BOOTMODE)"

   exit 0
   ;;
esac
#
# This function does general clean-up and reporting in case of errors.
# If we ever get here, there was a problem.  Keep retrying the update
# until the boot flag is cleared.
#
quit() {
   echo "BOOTMODE.SH::quit: $2"

   ########### TODO: Fix this , RESET the box 
   # start mmc driver 
   if [ -e /bin/mmc.sh ]; then /bin/sh /bin/mmc.sh start application; fi

#   /bin/sh /bin/dev-ipc.sh start
   
   if [ -x /bin/start_display ]; then
      /bin/start_display
   else
      screen
      waitfor /dev/screen
   fi
   
   if [[ $1 -ne 6 ]]; then
      splash /etc/waitForUsb.png &
   else
      echo "BOOTMODE.SH: Quit because memifs2 failed - corrupt ifs"
      splash /etc/invalidUpdate.png &
   fi

   startBackLight   
   
   sleep 10
   # create a file/variable under temp and read the value from FRAM(swdl)
   # Store in COUNTER
   # *** NOTE ****#
   # 124th byte of the swdl in FRAM is used to store the counter values.
   # mmc and etfs are not used since at this time these are not available.
   # *************
      
   touch /tmp/value
   inject -e -i /dev/mmap/swdl -f /tmp/value -o 124 -s 1 
   COUNTER=`cat /tmp/value`

   # more than 7 times reset in the "swdl" of FRAM
   if [[ $COUNTER -gt 7 ]]; then
      echo "BOOTMODE.SH: Send the Shutdown command to IOC as system restarted for more than 7 times"
      # reset the value to '0' in /dev/mmap/swdl
      COUNTER=0
      echo $COUNTER >/tmp/value
      inject -i /tmp/value -f /dev/mmap/swdl  -o 124 -s 1
      # call the lua function send the shutdown command to IOC
      /bin/shutdown.lua
      rm -f /tmp/value
   else
      echo "BOOTMODE.SH: Incrment SHUTDOWN_COUNTER"
      echo $(($COUNTER+1))>/tmp/value
      inject -i /tmp/value -f /dev/mmap/swdl  -o 124 -s 1 
      echo $COUNTER
      # stay in V850 bolo mode if we are in update mode
      echo "Stay in BOLO MODE"
      bolo
   fi
   
   # Reset will probably return, so wait for a reset forever...
   while true; do
     sleep 10
   done
  }
 
echo "BOOTMODE.SH: Software Update Mode Detected"

# make sure ifs 2 is loaded
waitfor /lib/libssl.so.2 2

# Show "pleaseWait" image during installer.iso copy till HMI is ready
if [ -x /bin/start_display ]; then
  /bin/start_display
else
  screen
  waitfor /dev/screen
fi
splash /etc/pleaseWait_3_languages.png &
startBackLight 

#
# Check v850 Mode ( app or bolo) and verify it is same as expected
# if not put this into expected mode, only reason to do this is so that 
# if some usb sticks took longer than expected to verify V850 watchdog won't 
# reset the system if V850 is in application mode
#
# Also, evaluate the output so that we have the IOC mode. If we're in
# application IOC mode, then we need to start the watchdog.
#

# reset if not at expected mode then start the watchdog if in app mode
/bin/checkSwdlV850Mode.lua
if [ $? -eq 0 ]; then
   echo "BOOTMODE.SH: Starting watchdog"
   # perhaps we should just do the check for the mode we are in in the watchdog and exit if not in app mode
	lua -s /bin/watchdogV850.lua &
else
   echo "BOOTMODE.SH: Not starting watchdog"
fi

#
# Define the most basic part of the USB stick layout and where the
# ISO will be mounted.  Use my PID and a random number in the
# mount path to insure no collisions with anything already
# mounted on the target.
#
USB_STICK=/fs/usb0
ISO_NAME=swdl.upd
ISO_PATH=/mnt/swdl.upd.$$.$RANDOM
ISO_INSTALLER_PATH=/mnt/installer.iso.$$.$RANDOM
ISO_PRIMARY_PATH=/mnt/primary.iso.$$.$RANDOM
ISO_SECONDARY_PATH=/mnt/secondary.iso.$$.$RANDOM
MMC_SWDL_PATH=/fs/mmc0
KEYS_DIR=/etc/keys

#
# Get the USB stick mounted.  Large USB devices can take
# some number of seconds to enumerate and auto mount.
#
enum-devices -c /etc/system/enum/common
#  Start the usb overcurrent monitor utility
/usr/bin/usb_hub_oc -p 10 -i 10 -d 500 &
waitfor $USB_STICK/$ISO_NAME 30

#
# Locate the ISO either in the root of the usb stick or in the
# old location of swdl/swdl.upd.  We will exit if we cannot find
# a suitable ISO image.
#
if [ -e $USB_STICK/$ISO_NAME ]; then
  ISO=$USB_STICK/$ISO_NAME
else
  quit 1 "Could not locate update ISO image on $USB_STICK."
fi

echo "BOOTMODE.SH: SWDL ISO found at $ISO"

#
# Mount the ISO from the USB stick
#
mount -t cd -o exe $ISO $ISO_PATH > /dev/null 2>&1
if [ $? -ne 0 ]; then
  quit 3 "Could not mount update ISO image."
fi
echo "BOOTMODE.SH: Mounted" $ISO on $ISO_PATH

if [ -e /bin/mmc.sh ]; then
   # stop the mmc driver
   /bin/sh /bin/mmc.sh stop
   if [[ $? -ne 0 ]]; then
      quit 5 "BOOTMODE.SH: Error stopping mmc driver"
   fi
   
   # Start the mmc driver in update mode for SWDL
   /bin/sh /bin/mmc.sh start
   if [[ $? -ne 0 ]]; then
      quit 5 "BOOTMODE.SH: Error starting mmc driver"
   fi
fi

waitfor $MMC_SWDL_PATH

if [ -e $MMC_SWDL_PATH/ISO_COPY_MMC_AUTHENTICATED ]; then
   echo "BOOTMODE.SH: Skipping installer ISO copy & authentication as it's already done"
else
	# Remove installer.iso and primary.iso from MMC if they were already present
	if [ -e $MMC_SWDL_PATH/installer.iso ]; then
	   rm -f $MMC_SWDL_PATH/installer.iso
	fi
	if [ -e $MMC_SWDL_PATH/primary.iso ]; then
	   rm -f $MMC_SWDL_PATH/primary.iso
	fi

	# Copy installer iso to mmc
	echo "Copying" $ISO_PATH"/installer.iso to "$MMC_SWDL_PATH " ,it will take some time please wait..."
	dd if=$ISO_PATH/installer.iso of=$MMC_SWDL_PATH/installer.iso seek=0 skip=0
	sync
	echo "Copy Done for installer.iso from USB to"$MMC_SWDL_PATH

	#
	# Authenticate the ISO before mounting it.
	#

	COMPHDR_TMP=/tmp/32k.bin
	ISOHDR_TMP=/tmp/isoheader.bin
	SIGNATURE_TMP=/tmp/32kdigsig.bin
	ISODATA_HASH=/tmp/isohash.bin
	EXTRACTED_HASH=/tmp/extractedisohash.bin
	CALCULATED_HASH=/tmp/calculatedisohash.bin
	SWDL_PUB_KEY=/etc/keys/swdl.pub

	# Copy the 32K header to /tmp
	dd if=$MMC_SWDL_PATH/installer.iso of=$COMPHDR_TMP ibs=32768 count=1 > /dev/null 2>&1
  
	# Copy the ISO Header Dig Signature (first 256 bytes) from ISO to /tmp 
	dd if=$COMPHDR_TMP of=$SIGNATURE_TMP ibs=256 count=1 > /dev/null 2>&1

	# Copy the ISO Header Dig Signature (32K less the first 256 bytes) from ISO to /tmp 
	dd if=$COMPHDR_TMP of=$ISOHDR_TMP ibs=256 count=127 skip=1 > /dev/null 2>&1

	# Extract the Hash of ISO Data portion
	dd if=$MMC_SWDL_PATH/installer.iso of=$ISODATA_HASH ibs=256 count=1 skip=127 > /dev/null 2>&1

	echo "BOOTMODE.SH: Will authenticate ISO"

	openssl dgst -sha256 -verify $SWDL_PUB_KEY -signature $SIGNATURE_TMP $ISOHDR_TMP > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	  quit 2 "Could not authenticate the update ISO image"
	fi

	openssl rsautl -verify -inkey $SWDL_PUB_KEY -in $ISODATA_HASH -pubin -out $EXTRACTED_HASH > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	  quit 2 "Could not verify ISO Data Hash"
	fi

	hashFile sha256 $MMC_SWDL_PATH/installer.iso $CALCULATED_HASH 32768 0 0 0 > /dev/null 2>&1
	if [ $? -ne 0 ]; then
	  quit 2 "hashFile on Install ISO failed"
	fi

	cmp -s $CALCULATED_HASH $EXTRACTED_HASH
	if [ $? -ne 0 ]; then
	  quit 2 "ISO Data Section hash mismatch"
	fi


	rm -f $COMPHDR_TMP
	rm -f $ISOHDR_TMP
	rm -f $SIGNATURE_TMP 
	rm -f $ISODATA_HASH
	rm -f $EXTRACTED_HASH
	rm -f $CALCULATED_HASH

	echo "BOOTMODE.SH: ISO Authenticated"
   
   # /tmp/INSTALLER_ISO_AUTHENTICATED flag is used to synchronize ISO_COPY_MMC_AUTHENTICATED skipping
   # between bootmode.sh and install.sh
   # ELVIS#1925439 Unable to locate binary files
   touch /tmp/INSTALLER_ISO_AUTHENTICATED
fi

# Get the ISO Size for install monitor since Lua io.open for files > 2Gb will fail
# Need INSTALLER_ISOSIZE for detecting imhf.txt file in installer.iso
INSTALLER_ISOSIZE=`ls -l $MMC_SWDL_PATH/installer.iso | awk '{ print $5}'`
echo "INSTALLER_ISOSIZE=" $INSTALLER_ISOSIZE
#Need ISOSIZE_HASH for install monitor background scanner 
if [ -e $ISO_PATH/secondary.iso ]; then
  ISOSIZE_HASH=`ls -l $ISO_PATH/secondary.iso | awk '{ print $5}'`
  echo "ISOSIZE_HASH=" $ISOSIZE_HASH
else
  ISOSIZE_HASH=0
fi

/bin/sh /bin/dev-ipc.sh start
   
#
# Mount the ISO from the MMC
#
mount -t cd -o exe $MMC_SWDL_PATH/installer.iso $ISO_INSTALLER_PATH > /dev/null 2>&1
if [ $? -ne 0 ]; then
   quit 3 "Could not mount update Installer ISO image."
fi
echo "BOOTMODE.SH: Mounted" $MMC_SWDL_PATH/installer.iso on $ISO_INSTALLER_PATH

#
# Load the system executables from the IFS within
# the ISO.
#
if [ -e $ISO_INSTALLER_PATH/usr/share/swdl.bin ]; then
  memifs2 -q -d $ISO_INSTALLER_PATH/usr/share/swdl.bin /
  retval=$?
  if [[ $retval -ne 0 ]]; then
     echo memifs2 returned $?
     quit 6 "memifs2 could not load swdl.bin"
  fi
else
  quit 4 "No IFS in ISO."
fi

#
# Call the install script
#
if [ -x $ISO_INSTALLER_PATH/usr/share/scripts/install.sh ]; then
  export ISO_PATH
   export ISO_INSTALLER_PATH
   export ISO_PRIMARY_PATH
   export ISO_SECONDARY_PATH
   export MMC_SWDL_PATH
  export USB_STICK

  echo "BOOTMODE.SH: Running the installer..."

  # /bin/ksh -i

   $ISO_INSTALLER_PATH/usr/share/scripts/install.sh $INSTALLER_ISOSIZE $ISOSIZE_HASH
   RET_VAL=$?
   if [ $RET_VAL == 1 ]; then
     quit 7 "Bootmode_sh : install_sh Copy Failed for primary.iso"
   elif  [ $RET_VAL == 2 ]; then
     quit 7 "Bootmode_sh : install_sh Verification isochk failed"
   elif  [ $RET_VAL == 3 ]; then
     quit 7 "Bootmode_sh : install_sh Could not mount update primary ISO"
   elif  [ $RET_VAL == 4 ]; then
     quit 7 "Bootmode_sh : install_sh Could not mount update secondary ISO"
   fi
   
else

  quit 3 "No install script found."
fi

#
# We never get here except for testing!
#
quit 0 "Done."
