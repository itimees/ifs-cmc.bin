#!/usr/bin/lua

-- local onoff = require "onoff"
local timer = require "timer"
local ipc = require "ipc"

local chan2 = assert(ipc.open(2))

local wdMsg = {}
wdMsg[1] = 0x02
wdMsg[2] = 0x01
wdMsg[3] = 0x00
wdMsg[4] = 0x00

---------------------------
-- Start watchdog timer
---------------------------
local function petWatchdog()
   chan2:write( wdMsg)
end

--
-- Send the watchdog message every half second
--
local watchdogTimer = timer.new(petWatchdog)
watchdogTimer:start(500)


local function initComplete()
   print( "Changing WD message from init to running")
   wdMsg[2] = 0x02
   wdMsg[3] = 0x03
   wdMsg[4] = 0x03
end

--
-- After the first three, update the message from
-- initializing to running state
--
local initCompleteTimer = timer.new( initComplete)
initCompleteTimer:start(1750, 1)

