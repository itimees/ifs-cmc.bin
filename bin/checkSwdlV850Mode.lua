#!/usr/bin/lua

-- return 0 if we are in ap mode and 1 if not

local onoff     = require "onoff"
local os        = os

-- if the IOC is in app mode then exit with 0 else exit with 1
local bootmode, err= onoff.getIocBootMode()

if err then print("checkSwdlV850Mode: ", err) end

if (bootmode==onoff.IOCAPPMODESTRING) then
   os.exit(0)
else
   os.exit(1)
end
