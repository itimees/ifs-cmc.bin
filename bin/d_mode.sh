#!/bin/sh

#
# This function does general clean-up and reporting in case of errors.
# If we ever get here, there was a problem.  Keep retrying the update
# until the boot flag is cleared.
#
quit() {
   echo "D_MODE.SH::quit: $2"

   if [[ $1 -ne 0 ]]; then
      if [ -x /bin/start_display ]; then
         /bin/start_display
      else
         screen
         waitfor /dev/screen
      fi

      echo "D_MODE.SH: FOTA Update Failed"
      #TODO: Get correct image from FCA to show the error
      splash /etc/fotaUpdateFail.png &
      
      startBackLight
      sleep 10
   fi

   bolo

# Reset will probably return, so wait for a reset forever...
   while true; do
      sleep 10
   done
}

loadtertiaryifs()
{
   echo D_MODE.SH: memifs2 with 4-bit ECC including spare area
	# (use this if loading with etfs not already loaded) memifs2 -q -N -e 2 -n /
   memifs2 -p 3 -q -N -e 2 -n /
   if [[ 0 != $? ]] ; then
      echo "D_MODE.SH: **********Failure while loading tertiary IFS**********" > /dev/ser3
      echo "D_MODE.SH: **********Marking current IFS as invalid ***********"
      qwaitfor /dev/mmap/
      adjustImageState -c 1
      if [[ 0 == $? ]] ; then
         echo "D_MODE.SH: **********Image state set to bad************"
      else
         echo "D_MODE.SH: *******Unable to adjust image state - FRAM not available********"
      fi
      #echo "**********Resetting the hardware********************"
      #echo -n "\\0021" > /dev/ipc/ch4
      quit 5 "D_MODE.SH: loadtertiaryifs failed"
   else
      echo "D_MODE.SH: Tertiary IFS loaded successfully"
   fi
}

loadquadifs()
{
   echo D_MODE.SH: memifs2 with 4-bit ECC including spare area
	# (use this if loading with etfs not already loaded) memifs2 -q -N -e 2 -n /
   memifs2 -q -l 40 -p 4 -N -e 2 -n /
   if [[ 0 != $? ]] ; then
      echo "D_MODE.SH: **********Failure while loading quaternary IFS**********" > /dev/ser3
      echo "D_MODE.SH: **********Marking current IFS as invalid ***********"
      qwaitfor /dev/mmap/
      adjustImageState -c 1
      if [[ 0 == $? ]] ; then
         echo "D_MODE.SH: **********Image state set to bad************"
      else
         echo "D_MODE.SH: *******Unable to adjust image state - FRAM not available********"
      fi
      adjustImageState -c 1
      #echo "**********Resetting the hardware********************"
      #echo -n "\\0021" > /dev/ipc/ch4
      quit 5 "D_MODE.SH: loadtertiaryifs failed"
   else
      echo "D_MODE.SH:  quad IFS loaded successfully"
   fi
}

startEtfsDriver()
{
   # make sure new ETFS driver available in MMC
   waitfor $NEW_ETFS_DRIVER
   if [[ $? -ne 0 ]]; then
      quit 5 "D_MODE.SH: $NEW_ETFS_DRIVER was not loaded in mmc can't proceed for update"
   fi
   ###   echo starting ETFS driver............
   $NEW_ETFS_DRIVER -c 1024 -D cfg -m/fs/etfs -f /etc/system/config/nand_partition.txt
   waitfor /fs/etfs
   if [[ $? -ne 0 ]]; then
      quit 5 "D_MODE.SH: /fs/etfs was not loaded can't proceed for update"
   fi
}

executeDependency()
{
   if [ -e /bin/mmc.sh ]; then
      # stop the mmc driver
      /bin/sh /bin/mmc.sh stop
      if [[ $? -ne 0 ]]; then
         quit 5 "D_MODE.SH: Error stopping mmc driver"
      fi

      # Start the mmc driver in update mode for FOTA
      if [ $1 = "fota2Special" ]; then 
         # Start MMC without allowing it for MMC repartitioning, so that we can do the IOC ASAP i.e., with in 10min of V850 Shutdown timer
         # After V850 update, new V850 will remove the shutdown timer and we will not have a problem of HU shutdown during FOTA2
         /bin/sh /bin/mmc.sh start fota2Special
         if [[ $? -ne 0 ]]; then
            quit 5 "D_MODE.SH: Fota2 Special: Error starting mmc driver"
         fi
      else
         /bin/sh /bin/mmc.sh start
         if [[ $? -ne 0 ]]; then
            quit 5 "D_MODE.SH: Error starting mmc driver"
         fi
      fi
   fi
      
   #if rb_ua is loaded then mmc is loaded for D operation
   waitfor $FOTA_PATH/rb/ota/rb_ua
   if [[ $? -ne 0 ]]; then
      quit 5 "D_MODE.SH: $FOTA_PATH/rb/ota/rb_ua was not loaded can't proceed for update"
   fi

   # Prefer ISO binaries over those on target
   export PATH=$FOTA_INSTALLER_PATH/bin:$FOTA_INSTALLER_PATH/usr/bin:$FOTA_INSTALLER_PATH/usr/sbin:/bin:/usr/bin:/sbin:/usr/sbin:/opt/sys/bin
   export LD_LIBRARY_PATH=$FOTA_INSTALLER_PATH/lib:$FOTA_INSTALLER_PATH/lib/dll:/usr/lib/graphics/omap3730:/lib:/lib/dll:/usr/lib:/usr/lib/dll:/usr/lib/lua
   export LUA_PATH="$FOTA_INSTALLER_PATH/usr/share/scripts/update/?.lua;$FOTA_INSTALLER_PATH/usr/share/scripts/update/installer/?.lua;/usr/bin/?.lua;/usr/bin/?/init.lua";

   qln -sfP $FOTA_INSTALLER_PATH/bin/devc-ser8250 /bin/devc-ser8250
   qln -sfP $FOTA_INSTALLER_PATH/usr/share/hmi/Update/bin/ /usr/share/hmi/Update/bin/

   #TODO need to find out who need below services, if we remove this then VARIANT checking was failing for XM,..etc
   # Create link to hmiVars file for HMI to read out current language setting
   ln -fsP /fs/fram/hmi /usr/share/hmi/Update/bin/hmiVars

   random -t -p &
   # Start io-pkt
   io-pkt-v4 -ptcpip

   # Start dbus
   nice -n-2 dbus-launch --sh-syntax --config-file=/etc/dbus-1/session.conf > /tmp/envars.sh
   echo "export SVCIPC_DISPATCH_PRIORITY=12;" >> /tmp/envars.sh
   . /tmp/envars.sh

   # start dev-ipc 
   /bin/dev-ipc.sh start

   # set the environment variables for Part number
   eval `variant export`

   # Start hmiGateway
   nice -n-1 hmiGateway -sj &

   # TODO: waitfor is commented out because of bootloader bug 
   # this will open and close channel 4 which was causing problems
   # eventually will remove this and will put the waitfor 
   #waitfor /dev/ipc/ch4 20 
   sleep 2
}

handleSpecialFota2IOCUpdate()
{
   if [[ (-e /fs/etfs/HANDLE_SPECIAL_FOTA2_UPDATE) ]]; then   
      echo "D_MODE.SH: Start executing Special FOTA2 Update"
      executeDependency "fota2Special"
      
      if [[ (-e /fs/mmc0/dupd/fota_installer/usr/share/scripts/fota_custom_install_ioc.sh) ]]; then 
      /bin/sh /fs/mmc0/dupd/fota_installer/usr/share/scripts/fota_custom_install_ioc.sh
      echo "D_MODE.SH: Completed executing Special FOTA2 Update"
      else
         echo "D_MODE.SH: ERROR!!! HANDLE_SPECIAL_FOTA2_UPDATE flag present but fota_custom_install_ioc.sh file not available"
         rm /fs/etfs/HANDLE_SPECIAL_FOTA2_UPDATE
         sync
      fi

      #IOC fix applied, Reset HU so that we can do the regular FOTA2 update MMC+FW
      bolo
      while true; do
         sleep 10
      done
   fi
}

############################################ Start of d_mode.sh ################################################
# Run the differential update code
echo "D_MODE.SH: DUPD: PROCESSING DIFFERENTIAL UPDATE"

FOTA_PATH=/fs/mmc0/dupd
FOTA_INSTALLER_PATH=$FOTA_PATH/fota_installer
export FOTA_INSTALLER_PATH
FOTA_FW_PATH=/fs/mmc0/fota_fw
export FOTA_FW_PATH
NEW_ETFS_DRIVER=/bin/fs-etfs-omap3530_micron

# make sure ifs 2 is loaded
waitfor /lib/libssl.so.2 2
if [[ $? -ne 0 ]]; then
   quit 5 "D_MODE.SH: /lib/libssl.so.2 or ifs 2 was not loaded can't proceed for update"
fi

# Show "pleaseWait" image UpdateAgent HMI is ready
if [ -x /bin/start_display ]; then
  /bin/start_display
else
  screen
  waitfor /dev/screen
fi
splash /etc/cmc_fiat_please_wait_5to10.png &
startBackLight 

#
# Check v850 Mode ( app or bolo) and verify it is same as expected
# if not put this into expected mode, only reason to do this is so that 
# if some usb sticks took longer than expected to verify V850 watchdog won't 
# reset the system if V850 is in application mode
#
# Also, evaluate the output so that we have the IOC mode. If we're in
# application IOC mode, then we need to start the watchdog.
#

# reset if not at expected mode then start the watchdog if in app mode
/bin/checkSwdlV850Mode.lua
if [ $? -eq 0 ]; then
   echo "BOOTMODE.SH: Starting watchdog"
   # perhaps we should just do the check for the mode we are in in the watchdog and exit if not in app mode
	lua -s /bin/watchdogV850.lua &
else
   echo "BOOTMODE.SH: Not starting watchdog"
fi

# We need to launch below services as it's required for SIERRA_WIRELESS 
# Launch enum-devices before MMC so that we will not get the Network authentication errors if ethernet was connecetd
enum-devices -c /etc/system/enum/common
#  Start the usb overcurrent monitor utility
/usr/bin/usb_hub_oc -p 10 -i 10 -d 500 &

#Load all IFS before MMC sothat if any issue in IFS loading we can exit without modifying the MMC
loadtertiaryifs 

qwaitfor /bin/fs-etfs-omap3530_micron
if [[ $? -ne 0 ]]; then
   quit 5 "D_MODE.SH: /bin/fs-etfs-omap3530_micron or ifs 3 was not loaded can't proceed for update"
fi

#Start ETFS driver
startEtfsDriver

loadquadifs

# We should update IOC first during FOTA2 update to resolve the HU shutdown during FOTA2 D mode update
handleSpecialFota2IOCUpdate

# Launch/set all dependency required for update
executeDependency "fota"

# Start adl
waitfor /dev/screen
nice -n-1 adl -runtime /lib/air/runtimeSDK /usr/share/hmi/Update/bin/Update.xml &

# mqueue needs to be started before UA started as UA needs mqueue
/bin/mqueue

#launch rb_ua
$FOTA_PATH/rb/ota/rb_ua -c $FOTA_PATH/rb/ota/rb_ua.conf
# Ideally we should never come here
if [[ $? -ne 0 ]]; then
   quit 5 "D_MODE.SH: Delta Update Failed through rb_ua"
else
   quit 0 "D_MODE.SH: Update Done, but we should never come here"
fi
