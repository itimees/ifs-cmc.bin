#!/bin/sh
MAX_SIZE=2097152 #2MB


##############################
# Manage onOff.log filesize

fsize=`ls -la /usr/var/logs/onOff.log | sed -r 's/([^ ]+ +){4}([^ ]+).*/\2/'`

if [[ $fsize -gt MAX_SIZE ]]; then
   echo "[onOffLog] Filesize too large. Moving onOff.log to /fs/mmc1/logs/"

   mount -uw /fs/mmc1
   if [ ! -d /fs/mmc1/logs ]; then
      mkdir /fs/mmc1/logs
   fi
   mv /usr/var/logs/onOff.log /fs/mmc1/logs/$(date "+%d_%b_%Y")_onOff.log

   # Remount /fs/mmc1 to ready only
   mount -ur /fs/mmc1
fi

##############################
# Manage qdb_reset.log

qdbLogSize=`ls -la /usr/var/logs/qdb_reset.log | sed -r 's/([^ ]+ +){4}([^ ]+).*/\2/'`

#3kb
if [[ $qdbLogSize -gt 3072 ]]; then
   echo "[qdb_reset.log] Filesize too large. Moving qdb_reset.log to /fs/mmc1/logs/"

   mount -uw /fs/mmc1
   if [ ! -d /fs/mmc1/logs ]; then
      mkdir /fs/mmc1/logs
   fi
   mv /usr/var/logs/qdb_reset.log /fs/mmc1/logs/$(date "+%d_%b_%Y")_qdb_reset.log

   # Remount /fs/mmc1 to ready only
   mount -ur /fs/mmc1
fi
# reset the value to '0' in /dev/mmap/swdl
touch /tmp/value
COUNTER=0
echo $COUNTER >/tmp/value
inject -i /tmp/value -f /dev/mmap/swdl  -o 124 -s 1
   
# As SWDL installation is already completed, remove installer.iso and primary.iso from MMC if they were present
MMC_SWDL_PATH=/fs/mmc0
if [ -e $MMC_SWDL_PATH/installer.iso ]; then
   mount -uw $MMC_SWDL_PATH
   
   rm -f $MMC_SWDL_PATH/ISO_COPY_MMC_AUTHENTICATED
   rm -f $MMC_SWDL_PATH/TOTAL_PREUNITS
   rm -f $MMC_SWDL_PATH/imhf.txt
   
   rm -f $MMC_SWDL_PATH/installer.iso
   echo "Clean Up, removed "$MMC_SWDL_PATH"/installer.iso"
   mount -ur $MMC_SWDL_PATH
fi
if [ -e $MMC_SWDL_PATH/primary.iso ]; then
   mount -uw $MMC_SWDL_PATH
   rm -f $MMC_SWDL_PATH/primary.iso
   echo "Clean Up, removed "$MMC_SWDL_PATH"/primary.iso"
   mount -ur $MMC_SWDL_PATH
fi

