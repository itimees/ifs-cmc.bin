#!/usr/bin/lua
 
local ipc               = require "ipc"
local onoff             = require "onoff"
local os                = os
local respMsg           = {}
local interfaceVersion  = 0x02
 
-- if the IOC is in app mode then exit with 0 else exit with 1
local bootmode, err=onoff.getIocBootMode()
  
if err then print("shutdown.lua: ", err) end
  -- if IOC in app mode, then also shut it down.
  -- IOC does not accept the same interface of onoff during the IOC bolo mode.
if (bootmode==onoff.IOCAPPMODESTRING) then
   print("shutdown.lua:forcing the IOC to go the bootloader mode by sending bolo command")
   os.execute("bolo")
else
   print("shutdown.lua:IOC is in bolomode")
   -- Open the channel 4
   local ch4 = ipc.open(4,"rw")
   
   if ch4 == nill then
      print("shutdown.lua  - ERROR : Unable to open IPC channel 4")
   else
      print("shutdown.lua  - SUCCESS : able to open IPC channel 4")
      
      print("shutdown.lua  - Send the Shutdown cmd to IOC")
      -- 0x13 is IPC command to IOC to shutdown the Headunit
      local ret = ch4:write(0x13)
      
      if ret == 1 then 
         print("shutdown.lua : SUCCESS - Shutdown to command to IOC is successful")
      else
         print("shutdown.lua : ERROR - Shutdown to command to IOC failed")
      end
   end
end