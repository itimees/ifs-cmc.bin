# file evaluated if serial console or telnet session and current user
# evaluated second

if [[ $PPID -eq 1 ]]; then
   echo "Using serial console"

   if [[ -e /fs/etfs/HPSHELL ]]; then
      echo "Making the serial shell higher priority (~235)"
      renice -225 -p $$ 
   fi

else
   echo "Using network console"
fi

if [ -d /fs/etfs/development ]; then
   echo "Adding /fs/etfs/development directory to PATH ..."
   export PATH=$PATH:/fs/etfs/development
   echo "Adding /fs/etfs/development directory to LUA_PATH ..."
   export LUA_PATH="$LUA_PATH;/fs/etfs/development/?.lua"
   echo "Adding /fs/etfs/development directory to LD_LIBRARY_PATH ..."
   export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/fs/etfs/development

   if [ -e /dev/io-net/en0 ] && [ ! -e /tmp/qconn.pid ]; then
      qconn &
      if [ $? -ne 0 ] ;  then
         echo "waited for 5 seconds, but qconn did not start"
      else
         echo $! > /tmp/qconn.pid
         echo "qconn enabled"
      fi
         
      # open debug ports (see pf_a_en.conf)
      pfctl -a a_en -f /etc/pf_a_en.conf

   fi
   
   # add VARIANT_* to login
   eval `variant export`
fi

if [ -e /tmp/envars.sh ]; then
. /tmp/envars.sh
export DBUS_SESSION_BUS_ADDRESS, DBUS_SESSION_BUS_PID, SVCIPC_DISPATCH_PRIORITY
fi

export VISUAL=/fs/mmc0/app/bin/vi
export EDITOR=/fs/mmc0/app/bin/vi
